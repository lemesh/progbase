#include "stdafx.h"

int main(int argc, char *argv[]) {
    if (argc == 2) {
        if (!strcmp(argv[1], "-t")) {
            asserts();
            return EXIT_SUCCESS;
        }
    }
    int menu = 1;
    Console_hideCursor();
    char buffer[BUFFER_LEN] = {'\0'};
    int key = 0;
    while (true) {
        Console_clear();
        Console_setCursorPosition(0,0);
        if (menu == 1) printFormatMenuPoint("Create new empty struct!");
        else printf("Create new empty struct!\n");
        if (menu == 2) printFormatMenuPoint("Read struct from file");
        else printf("Read struct from file\n");
        printf("\nBackspace -> quit\n");
        key = Console_getChar();
        if (key == 65) menu--;
        if (key == 66) menu++;
        if (key == 127) return EXIT_SUCCESS;
        if (key == 10 && menu == 1) createEmpty();  
        if (key == 10 && menu == 2) readFromFile();      
        if (menu > 2) menu = 2;
        if (menu < 1) menu = 1;
    }
    return EXIT_SUCCESS; 
}

void createEmpty() {
    struct Capital *capitals[STRUCT_SIZE] = {NULL};
    menu(capitals, capitals);
}

int deleteCapitalFromPosition(struct Capital * capitals[], int pos) {
    int count = countOfCapitals(capitals);
    if (pos >= STRUCT_SIZE || pos >= count) return 1;
    if (capitals[pos] != NULL) {
        free(capitals[pos]);
    }
    for (int i = pos; i < count - 1;i++) {
        capitals[i] = capitals[i + 1];
        capitals[i]->position = i;
    }
    capitals[count - 1] = NULL;
    return 0;
}

void menu(struct Capital * capitals[], struct Capital * print[]) {
    int menu = 1;
    int key = 0;
    while (true) {
        Console_clear();
        printCapitals(print);
        Console_setCursorPosition(0,0);
        if (menu == 1) printFormatMenuPoint("Add struct to the end");
        else printf("Add struct to the end\n");
        if (menu == 2) printFormatMenuPoint("Delete struct from some position");
        else printf("Delete struct from some position\n");
        if (menu == 3) printFormatMenuPoint("Rewrite all array of struct");
        else printf("Rewrite all items of struct in some position\n");
        if (menu == 4) printFormatMenuPoint("Rewrite struct in some position");
        else printf("Rewrite some item of struct in some position\n");
        if (menu == 5) printFormatMenuPoint("Search all capitals with greater"
         " than input longitude");
        else printf("Search all capitals with greater than input longitude\n");
        if (menu == 6) printFormatMenuPoint("Save array of struct to file");
        else printf("Save array of struct to file\n");
        printf("\nBackspace -> quit\n");
        key = Console_getChar();
        if (key == 65) menu--;
        else if (key == 66) menu++;
        if (key == 127) break;
        else if (key == 10) callFunction(menu, capitals);
        if (menu > 6) menu = 6;
        else if (menu < 1) menu = 1;
    }
    freeCapitals(capitals);
}

void printFormatMenuPoint(char *str) {
    printf("-> ");
    Console_setCursorAttribute(BG_INTENSITY_BLACK);
    puts(str);
    Console_setCursorAttribute(BG_DEFAULT);
}

void callFunction(int func, struct Capital * capitals[]) {
    char buffer[1000];
    switch (func) {
        case 1: {
            long pop = 0;
            double sq = 0;
            float longtitude = 0, latitude = 0;
            bool truth = false;
            char buff[30];
            Console_clear();
            Console_showCursor();
            getName(buff);
            pop = population();
            sq = square();
            longtitude = lTitude("longtitude");
            latitude = lTitude("latitude");
            if(fillCapital(capitals, buff, pop, sq, longtitude, latitude) == 1) {
                warningText("It's full");
            }
            Console_hideCursor();
            Console_clear();
            break;
        }
        case 2: {
            int pos = -1;
            char line[10];
            pos = getPosition(line);
            if(deleteCapitalFromPosition(capitals, pos) == 1){
                warningText("This position is NULL or doesn't exist");
            }
            break;
        }
        case 3: {
            int pos = 0;
            char buff[30];
            long pop = 0;
            double sq = 0;
            float longtitude = 0, latitude = 0;
            bool truth = false;
            Console_clear();
            Console_showCursor();
            pos = getPosition();
            if(capitals[pos] == NULL) {
                warningText("This position is NULL or doesn't exist");
                break;
            }
            else capitals[pos] = NULL;
            getName(buff);
            pop = population();
            sq = square();
            longtitude = lTitude("longtitude");
            latitude = lTitude("latitude");
            fillCapital(capitals, buff, pop, sq, longtitude, latitude);
            Console_hideCursor();
            Console_clear();
            break;
        }
        case 4: {
            int pos = -1;
            pos = getPosition();
            if(capitals[pos] == NULL) {
                warningText("This position is NULL or doesn't exist");
                break;
            }
            int key = 0, menu = 1;
            while(true) {
                Console_clear();
                printf("Choose type of item you want edit\n\n");
                if (menu == 1) printFormatMenuPoint("Name");
                else printf("Name\n");
                if (menu == 2) printFormatMenuPoint("Population");
                else printf("Population\n");
                if (menu == 3) printFormatMenuPoint("Square");
                else printf("Square\n");
                if (menu == 4) printFormatMenuPoint("Longtitude");
                else printf("Longtitude\n");
                if (menu == 5) printFormatMenuPoint("Latitude");
                else printf("Latitude\n");
                key = Console_getChar();
                if (key == 65) menu--;
                else if (key == 66) menu++;
                else if (key == 10) {
                    editItem(capitals, pos, menu);
                    break;
                }
                if (menu > 5) menu = 5;
                else if (menu < 1) menu = 1;
            };
            break;
        }
        case 5: {
            Console_clear();
            if(capitals[0] == NULL) {
                warningText("Struct is empty");
                break;
            }
            int longtitude = 0.0;
            longtitude = lTitude("longtitude");
            int count = countOfCapitals(capitals);
            struct Capital * capital[STRUCT_SIZE] = {NULL};
            int k = 0;
            for (int i = 0; i < count; i++) {
                if (capitals[i]->coord.longtitude > longtitude) {
                    capital[k] = capitals[i];
                    k++;
                }
            }
            if (k==0) warningText("No such capitals");
            menu(capitals, capital);
            break;
        }
        case 6: {
            char filename[100];
            correctFile(filename);
            strcat(filename, ".txt\0");
            FILE* f = fopen(filename, "w");
            if (capitals[0] == NULL) {
                warningText("Struct is empty");
                fclose(f);
                break;
            }
            capital_sToString(capitals, buffer, 1000);
            fprintf(f, "%s", buffer);
            fclose(f);
            break;
        }
    }
    menu(capitals, capitals);
}

int getPosition() {
    int pos = -1;
    char line[10];
    do {
        Console_clear();
        Console_showCursor();
        printf("Please enter position of struct to delete\n-> ");
        fgets(line, 10, stdin);
        size_t last = strlen(line) - 1;
        if (line[last] == '\n') line[last] = '\0';
        if (fgetsNumCheck(line) == false) pos = -1;
        else sscanf(line, "%i", &pos);
        Console_clear();
        Console_hideCursor();
        Console_clear();
    } while (pos < 0);
    return pos;
}

void getName(char * buffer) {
    bool truth = false;
    do {
        printf("Please enter the name of capital\n-> ");
        fgets(buffer, 30, stdin);
        size_t last = strlen(buffer) - 1;
        if (buffer[last] == '\n') buffer[last] = '\0';
        truth = fgetsCheck(buffer);
        Console_clear();
    } while(truth == false);
}

long population() {
    long pop = 0;
    char line[10];
    do {
        printf("Please enter the population of your capital\n-> ");
        char line[10];
        fgets(line, 10, stdin);
        size_t last = strlen(line) - 1;
        if (line[last] == '\n') line[last] = '\0';
        if (fgetsNumCheck(line) == false) pop = 0;
        else sscanf(line, "%li", &pop);
        Console_clear();
    } while (pop < 1);
    return pop;
}

double square() {
    double square = 0.0;
    char line[10];
    do {
        char line[10];
        printf("Please enter the square of your capital\n-> ");
        fgets(line, 10, stdin);
        size_t last = strlen(line) - 1;
        if (line[last] == '\n') line[last] = '\0';
        if (fgetsNumCheck(line) == false) square = 0;
        else sscanf(line, "%lf", &square);
        Console_clear();
    } while (square < 1);
    return square;
}

bool fgetsCheck(char * buffer) {
    for (int i = 0; buffer[i] != '\0'; i++) {
        if(buffer[i] == '-' && i == 0) return false;
        if(!isalpha(buffer[i]) && (buffer[i] != '-' && i != 0))
            return false;
        if(isdigit(buffer[i])) return false;
    }
    if(buffer[0] == '\0') return false;
    return true;
}

bool fgetsNumCheck(char * buffer) {
    int i = 0;
    if (buffer[0] == '-') i = 1;
    for (; buffer[i] != '\0'; i++){
        if ((buffer[i] == '.' || buffer[i] == ',') && !isdigit(buffer[i + 1]))
            return false;
        else if(!isdigit(buffer[i]) && buffer[i] != '.' && buffer[i] != ',')
            return false;
    }
    return true;
}

int countOfCapitals(struct Capital * capitals[STRUCT_SIZE]) {
    int count = 0;
    for (int i = 0; i < STRUCT_SIZE; i++) {
        if (capitals[i] != NULL) count++;
    }
    return count;
}

void printCapitals(struct Capital * capitals[]) {
    int k = 1;
    int i = countOfCapitals(capitals);
    if (i == 0) {
        Console_setCursorPosition(k, 60);
        printf("NULL\n");
        return;
    }
    //if (i < 3) i = 3;
    for (int j = 0; j < STRUCT_SIZE; j++) {
        Console_setCursorPosition(k, 60);
        if(capitals[j] != NULL) {
            printf("-----------------------------------------------");
            k++;
            Console_setCursorPosition(k, 60);
            printf("%i. Capital name = %s", capitals[j]->position,
             capitals[j]->name);
            k++;
            Console_setCursorPosition(k, 60);
            printf("Population = %li Square = %.2lf",
             capitals[j]->population, capitals[j]->square);
            k++;
            Console_setCursorPosition(k, 60);
            printf("Coordinates:");
            k++;
            Console_setCursorPosition(k, 60);
            printf("%f°\t%f°", capitals[j]->coord.longtitude,
             capitals[j]->coord.latitude);
             k++;
            Console_setCursorPosition(k, 60);
            printf("-----------------------------------------------");
        }
        k++;
    }
}

int fillCapital(struct Capital * capitals[], char * name, long pop,
 double square, float longtitude, float latitude) {
    int i = countOfCapitals(capitals);
    if (i == 10) return 1;
    if (capitals[i] == NULL) {
        capitals[i] = malloc(sizeof(struct Capital));
        strcpy(capitals[i]->name, name);
        capitals[i]->population = pop;
        capitals[i]->square = square;
        capitals[i]->coord.latitude = latitude;
        capitals[i]->coord.longtitude = longtitude;
        capitals[i]->position = i;
    }
    return 0;
}

void editItem(struct Capital * capitals[], int pos, int item) {
    int count = countOfCapitals(capitals);
    if (pos >= count) return;
    Console_clear();
    if (item == 1) {
        char buffer[30];
        memset(capitals[pos]->name, '\0', 30);
        getName(buffer);
        strcpy(capitals[pos]->name, buffer);
    } else if (item == 2) {
        long pop = 0;
        pop = population();
        capitals[pos]->population = pop;
    } else if (item == 3) {
        double sq = 0.0;
        sq = square();
        capitals[pos]->square = sq;
    } else if (item == 4) {
        float longtitude = 0.0;
        longtitude = lTitude("longtitude");
        capitals[pos]->coord.longtitude = longtitude;
    } else if (item == 5) {
        float latitude = 0.0;
        latitude = lTitude("latitude");
        capitals[pos]->coord.latitude = latitude;
    }
}

float lTitude(char * str) {
    float lTitude = 0.0;
    char line[10];
    do {
        char line[10];
        printf("Please enter the %s of your capital\n-> ", str);
        fgets(line, 10, stdin);
        size_t last = strlen(line) - 1;
        if (line[last] == '\n') line[last] = '\0';
        if (fgetsNumCheck(line) == false) {
            lTitude = 101;
        }
        else sscanf(line, "%f", &lTitude);
        if (lTitude > 90 || lTitude < -90) {
            warningText("Such coordinate doesn't exist");
            lTitude = 101;
        }
        Console_clear();
    } while (lTitude > 90);
    return lTitude;
}

int capitalToString(struct Capital * capital, char * buffer, int bufferLength) {
    if(capital == NULL) return 0;
    int nwrite = snprintf(buffer, bufferLength, "%s#%li#%.2lf#%.2f %.2f", capital->name,
     capital->population, capital->square, capital->coord.longtitude,
     capital->coord.latitude);
    return nwrite;
}

void capital_sToString(struct Capital * capitals[STRUCT_SIZE], char * buffer,
 int bufferLength) {
    if (capitals[0] == NULL) return;
    int totalWrite = 0;
    for (int i = 0; i < STRUCT_SIZE; i++) {
        if(capitals[i] != NULL) {
            totalWrite += snprintf(buffer + totalWrite, bufferLength - totalWrite, "%i: ",
             capitals[i]->position);
            totalWrite += capitalToString(capitals[i], buffer + totalWrite,
             bufferLength - totalWrite);
            totalWrite += snprintf(buffer + totalWrite,
             bufferLength - totalWrite, "\n");
        }
    }
}

void readFromFile() {
    struct Capital * capitals[STRUCT_SIZE] = {NULL};
    char buffer[1000];
    char filename[100];
    correctFile(filename);
    strcat(filename, ".txt\0");
    FILE  *f = fopen(filename, "rb");
    if (!f) {
        warningText("Such file doesn't exist");
        return;
    }
    //fscanf(f, "%s", buffer);
    long nread = fread(buffer, 1, 1000, f);
    buffer[nread] = '\0';
    fclose(f);
    fillCapital_sFromString(capitals, buffer, nread);
    menu(capitals, capitals);
}

int fillCapitalFromString(struct Capital * capitals[],const char *str, int i) {
    int nread = 0;
    if (str[0] == '\0') return 1;
    if (capitals[i] == NULL) {
        capitals[i] = malloc(sizeof(struct Capital));
        sscanf(str, "%i%*c%*c%29[^#]%*c%li%*c%lf%*c%f%f%n", &(capitals[i]->position),
         capitals[i]->name,
        &(capitals[i]->population), &(capitals[i]->square),
        &(capitals[i]->coord.longtitude), &(capitals[i]->coord.latitude),
        &nread);
    }
    return nread;
}

void freeCapitals(struct Capital * capitals[STRUCT_SIZE]) {
    for (int i = 0; i < STRUCT_SIZE; i++) {
        if (capitals[i] != NULL)
            free(capitals[i]);
    }
}

void fillCapital_sFromString(struct Capital * capitals[STRUCT_SIZE], char * buffer, long nread) {
    int totalRead = 0;
    for (int i = 0; i < STRUCT_SIZE; i++) {
        if (totalRead <= nread) {
            totalRead += fillCapitalFromString(capitals, buffer + totalRead, i);
            totalRead++;
        } else break;
    }
}

bool structEquals(struct Capital * a, struct Capital * b) {\
    if (a == NULL && b == NULL) return true;
    if ((a == NULL && b != NULL) || (a != NULL && b == NULL)) return false;
    return (!strcmp(a->name,b->name) && a->population == b->population
    && fabs(a->square - b->square) < EPSILON
    && a->coord.latitude == b->coord.latitude
    && a->coord.longtitude == b->coord.longtitude);
}

bool capitalsEquals(struct Capital * a[], struct Capital * b[]) {
    if (countOfCapitals(a) != countOfCapitals(b)) return false;
    int size = countOfCapitals(a);
    for (int i = 0; i < size; i++) {
        if (structEquals(a[i], b[i]) == false)
            return false;
    }
    return true;
}

void warningText(const char * str) {
    Console_clear();
    Console_setCursorAttribute(BG_RED);
    printf("%s\n", str);
    Console_setCursorAttribute(BG_DEFAULT);
    sleepMillis(1000);
}

void correctFile(char *str) {
    bool truth = true;
    do {
        Console_clear();
        printf("Input name of file\n->");
        fgets(str, 100, stdin);
        size_t last = strlen(str) - 1;
        if (str[last] == '\n') str[last] = '\0';
        for (int i = 0; str[i] != '\0'; i++) {
            if(!isalpha(str[i]) && !isdigit(str[i])) {
                warningText("Incorrect input, filename exist only letters and digits");
                truth = false;
                break;
            }
        }
    } while(truth == false);
}

void asserts() {

    // asserts for fillCapitalFromString
    {
        struct Capital * as[10] = {NULL};
        fillCapital(as, "Kyiv", 3000, 300, 4, 4);
        fillCapitalFromString(as, "0: Kyiv#3000#300#4 4", 1);
        assert(
            structEquals(as[0], as[1]) == true
        );
        as[2] = NULL;
        fillCapitalFromString(as, "", 3);
        assert(
            structEquals(as[2], as[3]) == true
        );
        as[4] = NULL;
        fillCapitalFromString(as, "1:", 5);
        assert(
            structEquals(as[4], as[5]) == false
        );
        fillCapitalFromString(as, "1:", 6);
        as[7] = NULL;
        assert(
            structEquals(as[6], as[7]) == false
        );
        fillCapital(as, "", 0, 0, 0, 0);
        fillCapitalFromString(as, "2:", 9);
        assert(
            structEquals(as[8], as[9]) == false
        );
        freeCapitals(as);
    }
    //asserts for fillCapital_sFromString
    {
        {
            struct Capital * capitals[10] = {NULL};
            char * buffer = "0: Kyiv#3#3.00#3.00 3.00\n1: Kyiv#3"
            "#3.00#3.00 3.00\n2: Kyiv#3#3.00#3.00 3.00\n3: edasd#2#2.00#2.00 2.00";
            fillCapital_sFromString(capitals, buffer, 100);
            struct Capital * capitals2[10] = {NULL};
            capitals2[0] = malloc(sizeof(struct Capital));
            capitals2[0]->position = 0;
            strcpy(capitals2[0]->name, "Kyiv");
            capitals2[0]->population = 3;
            capitals2[0]->square = 3.00;
            capitals2[0]->coord.longtitude = 3.00;
            capitals2[0]->coord.latitude = 3.00;
            capitals2[1] = malloc(sizeof(struct Capital));
            capitals2[1]->position = 1;
            strcpy(capitals2[1]->name, "Kyiv");
            capitals2[1]->population = 3;
            capitals2[1]->square = 3.00;
            capitals2[1]->coord.longtitude = 3.00;
            capitals2[1]->coord.latitude = 3.00;
            capitals2[2] = malloc(sizeof(struct Capital));
            capitals2[2]->position = 2;
            strcpy(capitals2[2]->name, "Kyiv");
            capitals2[2]->population = 3;
            capitals2[2]->square = 3.00;
            capitals2[2]->coord.longtitude = 3.00;
            capitals2[2]->coord.latitude = 3.00;
            capitals2[3] = malloc(sizeof(struct Capital));
            capitals2[3]->position = 3;
            strcpy(capitals2[3]->name, "edasd");
            capitals2[3]->population = 2;
            capitals2[3]->square = 2.00;
            capitals2[3]->coord.longtitude = 2.00;
            capitals2[3]->coord.latitude = 2.00;
            assert(
                capitalsEquals(capitals, capitals2) == true
            );
            freeCapitals(capitals);
            freeCapitals(capitals2);
        }
        {
            struct Capital * capitals[10] = {NULL};
            char * buffer = "0: Kyiv#3#3.00#3.00 3.00\n1: Kyiv#3"
            "#3.00#3.00 3.00\n2: Kyiv#3#3.00#3.00 3.00\n3: edasd#2#2.00#2.00 2.00";
            fillCapital_sFromString(capitals, buffer, 100);
            struct Capital * capitals2[10] = {NULL};
            capitals2[0] = malloc(sizeof(struct Capital));
            capitals2[0]->position = 0;
            strcpy(capitals2[0]->name, "Kyiv");
            capitals2[0]->population = 3;
            capitals2[0]->square = 3.00;
            capitals2[0]->coord.longtitude = 3.00;
            capitals2[0]->coord.latitude = 3.00;
            capitals2[1] = malloc(sizeof(struct Capital));
            capitals2[1]->position = 1;
            strcpy(capitals2[1]->name, "Kyiv");
            capitals2[1]->population = 3;
            capitals2[1]->square = 3.00;
            capitals2[1]->coord.longtitude = 3.00;
            capitals2[1]->coord.latitude = 3.00;
            capitals2[2] = malloc(sizeof(struct Capital));
            capitals2[2]->position = 2;
            strcpy(capitals2[2]->name, "Kyiv");
            capitals2[2]->population = 3;
            capitals2[2]->square = 3.00;
            capitals2[2]->coord.longtitude = 3.00;
            capitals2[2]->coord.latitude = 3.00;
            assert(
                capitalsEquals(capitals, capitals2) == false
            );
            freeCapitals(capitals);
            freeCapitals(capitals2);
        }
        {
            struct Capital * capitals[10] = {NULL};
            char * buffer = "0: Kyiv#3#3.00#3.00 3.00\n1: Kyiv#3"
            "#3.00#3.00 3.00\n2: Kyiv#3#3.00#3.00 3.00\n";
            fillCapital_sFromString(capitals, buffer, 75);
            struct Capital * capitals2[10] = {NULL};
            capitals2[0] = malloc(sizeof(struct Capital));
            capitals2[0]->position = 0;
            strcpy(capitals2[0]->name, "Kyiv");
            capitals2[0]->population = 3;
            capitals2[0]->square = 3.00;
            capitals2[0]->coord.longtitude = 3.00;
            capitals2[0]->coord.latitude = 3.00;
            capitals2[1] = malloc(sizeof(struct Capital));
            capitals2[1]->position = 1;
            strcpy(capitals2[1]->name, "Kyiv");
            capitals2[1]->population = 3;
            capitals2[1]->square = 3.00;
            capitals2[1]->coord.longtitude = 3.00;
            capitals2[1]->coord.latitude = 3.00;
            capitals2[2] = malloc(sizeof(struct Capital));
            capitals2[2]->position = 2;
            strcpy(capitals2[2]->name, "Kyiv");
            capitals2[2]->population = 3;
            capitals2[2]->square = 3.00;
            capitals2[2]->coord.longtitude = 3.00;
            capitals2[2]->coord.latitude = 3.00;
            assert(
                capitalsEquals(capitals, capitals2) == true
            );
            freeCapitals(capitals);
            freeCapitals(capitals2);
        }
        {
            struct Capital * capitals[10] = {NULL};
            char * buffer = "0: Kyiv#3#3.00#3.00 3.00\n1: Kyiv#3"
            "#3.00#3.00 3.00";
            fillCapital_sFromString(capitals, buffer, 49);
            struct Capital * capitals2[10] = {NULL};
            capitals2[0] = malloc(sizeof(struct Capital));
            capitals2[0]->position = 0;
            strcpy(capitals2[0]->name, "Kyiv");
            capitals2[0]->population = 3;
            capitals2[0]->square = 3.00;
            capitals2[0]->coord.longtitude = 3.00;
            capitals2[0]->coord.latitude = 3.00;
            capitals2[1] = malloc(sizeof(struct Capital));
            capitals2[1]->position = 1;
            strcpy(capitals2[1]->name, "Kyiv");
            capitals2[1]->population = 3;
            capitals2[1]->square = 3.00;
            capitals2[1]->coord.longtitude = 3.00;
            capitals2[1]->coord.latitude = 3.00;
            assert(
                capitalsEquals(capitals, capitals2) == true
            );
            freeCapitals(capitals);
            freeCapitals(capitals2);
        }
        {
            struct Capital * capitals[10] = {NULL};
            char * buffer = "";
            fillCapital_sFromString(capitals, buffer, 0);
            struct Capital * capitals2[10] = {NULL};
            assert(
                capitalsEquals(capitals, capitals2) == true
            );
        }
    }
    // asserts for capitalToString
    {
        {
            char buffer[100];
            struct Capital capital;
            capital.position = 0;
            strcpy(capital.name, "Kyiv");
            capital.population = 3;
            capital.square = 3.00;
            capital.coord.latitude = 3.00;
            capital.coord.longtitude = 3.00;
            capitalToString(&capital, buffer, 100);
            assert(
                !strcmp(buffer, "Kyiv#3#3.00#3.00 3.00")
            );
        }
        {
            char buffer[100];
            struct Capital capital2;
            capital2.position = 0;
            strcpy(capital2.name, "Kyiv");
            capital2.population = 3;
            capital2.square = 3.00;
            capital2.coord.latitude = 3.00;
            capital2.coord.longtitude = 3.00;
            capitalToString(&capital2, buffer, 100);
            assert(
                strcmp(buffer, "1Kyiv#3#3.00#3.00 3.00") != 0
            );
        }
        {
            char buffer[100];
            struct Capital capital3;
            capital3.position = 0;
            strcpy(capital3.name, "Kyiv");
            capital3.population = 3;
            capital3.square = 3.00;
            capital3.coord.latitude = 3.00;
            capital3.coord.longtitude = 3.00;
            capitalToString(&capital3, buffer, 100);
            assert(
                strcmp(buffer, "Kyiv#2#3.00#3.00 3.00") != 0
            );
        }
        {
            char * buff = "";
            struct Capital * capital4 = NULL;
            capitalToString(capital4, buff, 100);
            assert(
                !strcmp(buff, "")
            );
            free(capital4);
        }
    }
    // asserts for capital_sToString
    {
        char buffer[1000];
        struct Capital * capitals2[10] = {NULL};
        capitals2[0] = malloc(sizeof(struct Capital));
        capitals2[0]->position = 0;
        strcpy(capitals2[0]->name, "Kyiv");
        capitals2[0]->population = 3;
        capitals2[0]->square = 3.00;
        capitals2[0]->coord.longtitude = 3.00;
        capitals2[0]->coord.latitude = 3.00;
        capitals2[1] = malloc(sizeof(struct Capital));
        capitals2[1]->position = 1;
        strcpy(capitals2[1]->name, "Kyiv");
        capitals2[1]->population = 3;
        capitals2[1]->square = 3.00;
        capitals2[1]->coord.longtitude = 3.00;
        capitals2[1]->coord.latitude = 3.00;
        capitals2[2] = malloc(sizeof(struct Capital));
        capitals2[2]->position = 2;
        strcpy(capitals2[2]->name, "Kyiv");
        capitals2[2]->population = 3;
        capitals2[2]->square = 3.00;
        capitals2[2]->coord.longtitude = 3.00;
        capitals2[2]->coord.latitude = 3.00;
        capitals2[3] = malloc(sizeof(struct Capital));
        capitals2[3]->position = 3;
        strcpy(capitals2[3]->name, "edasd");
        capitals2[3]->population = 2;
        capitals2[3]->square = 2.00;
        capitals2[3]->coord.longtitude = 2.00;
        capitals2[3]->coord.latitude = 2.00;
        capital_sToString(capitals2, buffer, 1000);
        assert(
            !strcmp(buffer, "0: Kyiv#3#3.00#3.00 3.00\n1: Kyiv#3#3.00#3.00 3.00"
            "\n2: Kyiv#3#3.00#3.00 3.00\n3: edasd#2#2.00#2.00 2.00\n")  
        );
        freeCapitals(capitals2);

        struct Capital * capitals[10] = {NULL};
        char buff[1000] = "";
        capital_sToString(capitals, buff, 1000);
        assert(
            !strcmp(buff, "")
        );
    }
    // deleteCapitalFromPosition
    {
        struct Capital * capitals[10] = {NULL};
        fillCapital_sFromString(capitals, "0: Kyiv#3#3.00#3.00 3.00\n1: Kyiv#3"
            "#3.00#3.00 3.00\n2: Kyiv#3#3.00#3.00 3.00\n3: edasd#2#2.00#2.00 2.00"
            , 100);
        deleteCapitalFromPosition(capitals, 2);
        struct Capital * capitals2[10] = {NULL};
        fillCapital_sFromString(capitals2, "0: Kyiv#3#3.00#3.00 3.00\n1: Kyiv#3"
            "#3.00#3.00 3.00\n2: edasd#2#2.00#2.00 2.00", 75);
        assert(
            capitalsEquals(capitals, capitals2) == true
        );
        freeCapitals(capitals);
        freeCapitals(capitals2);
    }
}