//#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <ctype.h>
#include <progbase.h>
#include <progbase/console.h>
#include <assert.h>

#define STRUCT_SIZE 10
#define BUFFER_LEN 100
#define EPSILON 1e-4

struct coordinates {
    float latitude;
    float longtitude;
}; 

struct Capital {
    int position;
    char name[30];
    long population;
    double square;
    struct coordinates coord;
};  

int fillCapitalFromString(struct Capital * capitals[],const char * str, int i);
void fillCapital_sFromString(struct Capital * capitals[STRUCT_SIZE], char * buffer, long nread);
void freeCapitals(struct Capital * capitals[STRUCT_SIZE]);
int capitalToString(struct Capital * capital, char * buffer, int bufferLength);
void capital_sToString(struct Capital * capitals[], char * buffer, int bufferLength);
void printCapitals(struct Capital * capitals[]);
bool structEquals(struct Capital * a, struct Capital * b);
void createEmpty();
void readFromFile();
void menu(struct Capital * capitals[], struct Capital * print[]);
void printFormatMenuPoint(char * str);
void callFunction(int func, struct Capital * capitals[]);
int countOfCapitals(struct Capital * capitals[]);
int fillCapital(struct Capital * capitals[], char * name, long pop,
 double square, float longtitude, float latitude);
int deleteCapitalFromPosition(struct Capital * capitals[], int pos);
bool fgetsCheck(char * buffer);
bool fgetsNumCheck(char * buffer);
double square();
long population();
float lTitude(char * str);
void getName(char * buffer);
int getPosition();
void editItem(struct Capital * capitals[], int pos, int item);
void warningText(const char *str);
void correctFile(char *str);
void asserts();
bool capitalsEquals(struct Capital * a[], struct Capital * b[]);