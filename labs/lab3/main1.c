#include <stdio.h>
#include <ctype.h>

char lastIndex(const char * str){
    if(str[0] == '\0'){
        return str[0];
    }
    else {
        const char temp = lastIndex(str + 1);
        return (isdigit(temp)) ? temp : str[0];
    }
} 

int main(){
    char * str = "ehoiqwekk";
    char temp = lastIndex(str);
    printf("%c", temp);
    return 0;
}