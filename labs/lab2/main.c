#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <progbase.h>
#include <math.h>
#include <progbase/console.h>

enum menuCommands {
    TASK1,
    TASK2,
    TASK3,
    TASK4,
    TASK5,
    TASK6,
    TASK7,
    HELP,
    QUIT
};

void randArray(int *, int, int, int);
enum menuCommands getMenuCommand();
int minIndex(int *, int);
int sumArray(int *, int);
long multiplyArray(int *, int);
void multiplyElement(int *, int, int);
int maxIndex(int *, int);
void swap(int *, int);
void swapMatrix(int *[], int);
void task1(int);
void task2(int);
void task3(int);
void printSegment(char *, char *, int, int);
void cleanArray(char *);

int main() {
    unsigned int menu = 0;
    unsigned int N = 10;
    do {
        Console_clear();
        printf("Hello, it is lab #2.\n\n"
        "1. Task #1 -- Operations with array\n"
        "2. Task #2 -- Operations with matrix\n"
        "3. Task #3 -- Operations with string\n"
        "4. Quit\n\n"
        "5. Change size(%i)\n"
        "Please, choose a task -> ", N);
        menu = getInt();
        if (menu == 1)
            task1(N);
        else if (menu == 2)
            task2(N);
        else if (menu == 3)
            task3(N);
        else if (menu == 4)
            printf("\nBye :(");
        else if (menu == 5) {
            Console_clear();
            printf("Print size of array, matrix or string -> ");
            N = getInt();
        }
    } while (menu != 4);
    puts("");
    return 0;
}

void task1(int N) {
    Console_clear();
    srand(time(0));
    int sum = 0;
    long multiply = 1;
    int index = -1;
    char line[] = "\n--------------------------------------------------------------------\n";
    int digitArray[N];
    for (int i = 0; i < N; i++)
        digitArray[i] = 0;
    int error = 0;
    unsigned int menu = 0;
    
    do {
        Console_clear();

        if(menu != HELP && menu != TASK6 && error != 1) {
            for (int i = 0; i < N; i ++) {
                printf("%i%c", digitArray[i], ' ');
            }
        }

        if (error == 1 && menu == TASK1) {
            Console_setCursorAttribute(BG_INTENSITY_RED);
            printf("You make a mistake -- your Low Limit is greater than High Limit");
            Console_setCursorAttribute(BG_DEFAULT);
            error = 0;
        } else if (error == 1 && menu == TASK7) { 
            printf("Please enter the number, not a letter or symbol");
            error = 0;
        } else if (menu == TASK3) {
            printf ("\n\nMin -> %i\tMinIndex -> %i", digitArray[index], index);
        } else if (menu == TASK4) {
            printf("\n\nSum of elements of array -> %i", sum);
            sum = 0;
        } else if (menu == TASK5) {
            if (multiply == 9173)
                printf("\n\nIt's all positive elements in array.");
            else
                printf("\n\nMultiply of negative elements of array -> %li", multiply);
            multiply = 1;
        } else if (menu == HELP) {
            Console_clear();
            printf("If this appear, it means that you need a help.\n");
            printf("It is a list of commands:\n");
            printf("help -- show list of commands\n");
            printf("quit -- close 'task1'");
        } else if (menu == TASK6) {
            Console_clear();
            int min = minIndex(digitArray, N);
            int max = maxIndex(digitArray, N);
            
            for (int i = 0; i < N; i ++) {
                if (i == min || i == max)
                    Console_setCursorAttribute(BG_INTENSITY_RED);
                printf("%i", digitArray[i]);
                Console_setCursorAttribute(BG_DEFAULT);
                printf(" ");
            }
        }

        puts(line);
        
        printf("List of task: \n"
        "1. Fill array by random elements\n"
        "2. Set elements of array to zero\n"
        "3. Find min element of array\n"
        "4. Sum of all elements\n"
        "5. Composition of negative elements\n"
        "6. Swap min and max element\n"
        "7. Multiply every element on some number\n\n");
        printf("Enter command, or number of task -> ");
        menu = getMenuCommand();
        switch (menu) {
            case TASK1: {
                int L = -1;
                int H = -1;
                printf("Please enter low limit of array -> ");
                L = getInt();
                
                printf("Please enter high limit of array -> ");
                H = getInt();

                if (L > H)
                    error = 1;
                else
                    randArray(digitArray, N, L, H);
                break;
            }
            case TASK2:
                for (int i = 0; i < N; i++)
                    digitArray[i] = 0;
                
                break;
            case TASK3:
                index = minIndex(digitArray, N);
                break;
            case TASK4:
                sum = sumArray(digitArray, N);
                break;
            case TASK5:
                multiply = multiplyArray(digitArray, N);
                break;
            case TASK6:
                swap(digitArray, N);
                break;
            case TASK7: {
                int number = 1;
                printf("\nPlease enter the number -> ");
                number = getInt();
                if (number != NAN)
                    multiplyElement(digitArray, N, number);    
                else error = 1;
                    
                break;
            }
            default:
                break;
        }
        Console_clear();
    } while (menu != QUIT);
}

void task2(int N) {
    int digitArray[N][N];
    int indexI = -1;
    int indexJ = -1;
    int indexMinJ = -1;
    int indexMaxJ = -1;
    int indexMinI = -1;
    int indexMaxI = -1;
    char line[] = "\n--------------------------------------------------------------------\n";
    int sum = 0;
    unsigned int r = 0;
    unsigned int c = 0;

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++)
            digitArray[i][j] = 0;
    }

    int menu = 0;
    int error = 0;

    do {
        Console_clear();
        if(menu != HELP && menu != TASK6 && error != 1 && menu != TASK7) {
            for (int i = 0; i < N; i ++) {
                for (int j = 0; j < N; j++)
                    printf("%i%c", digitArray[i][j], '\t');
                puts("");
            }
        }

        if (error == 1 && menu == TASK1) {
            Console_setCursorAttribute(BG_INTENSITY_RED);
            printf("You make a mistake -- your Low Limit is greater than High Limit");
            Console_setCursorAttribute(BG_DEFAULT);
            error = 0;
        } else if (menu == TASK3) {
            printf ("\n\nMax -> %i\tMaxIndex -> i = %i j = %i", digitArray[indexI][indexJ], indexI, indexJ);
        } else if (menu == TASK4) {
            printf("\n\nSum of elements of side diagonal -> %i", sum);
            sum = 0;
        } else if (menu == TASK5) {
            printf("\n\nSum of elements in this column -> %i", sum);
            sum = 0;
        } else if (menu == HELP) {
            Console_clear();
            printf("If this appear, it means that you need a help.\n");
            printf("It is a list of commands:\n");
            printf("help -- show list of commands\n");
            printf("quit -- close 'task2'");
        } else if (menu == TASK6) {
            Console_clear();
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {  
                    if ((i == indexMinI && j == indexMinJ) || (i == indexMaxI && j == indexMaxJ))
                        Console_setCursorAttribute(BG_INTENSITY_RED);
                    printf("%i", digitArray[i][j]);
                    Console_setCursorAttribute(BG_DEFAULT);  
                    printf("\t");
                }
                puts("");
            }
            Console_setCursorAttribute(BG_DEFAULT);
        } else if (menu == TASK7 && error == 0) {
            for (int i = 0; i < N; i ++) {
                for (int j = 0; j < N; j++) {
                    if( i == r - 1 && j == c - 1)
                        Console_setCursorAttribute(BG_INTENSITY_RED);
                    printf("%i", digitArray[i][j]);
                    Console_setCursorAttribute(BG_DEFAULT);
                    printf("\t");
                }
                puts("");
            }
        } else if (menu == TASK7 && error == 1) {
            Console_setCursorAttribute(BG_INTENSITY_RED);
            printf("Sorry, you go beyond the array\n");
            Console_setCursorAttribute(BG_DEFAULT);
            error = 0;
        }
        puts(line);
        
        printf("List of task: \n"
        "1. Fill matrix by random elements\n"
        "2. Set elements of matrix to zero\n"
        "3. Find max element of matrix\n"
        "4. Sum of elements by side diagonal\n"
        "5. Sum of elements in some column\n"
        "6. Swap min and max element\n"
        "7. Change number in some row and column\n\n");
        printf("Enter command, or number of task -> ");
        menu = getMenuCommand();
        switch (menu) {
            case TASK1: {
                int L = -1;
                int H = -1;
                printf("Please enter low limit of array -> ");
                L = getInt();
                
                printf("Please enter high limit of array -> ");
                H = getInt();

                if (L > H)
                    error = 1;
                else {
                    for (int i = 0; i < N; i++)
                    randArray(digitArray[i], N, L, H);
                }
                break;
            }
            case TASK2:
                for (int i = 0; i < N; i++) {
                    for (int j = 0; j < N; j++)
                        digitArray[i][j] = 0;
                }
                break;
            case TASK3: {
                int max = digitArray[0][0];
                int k = 0;

                for (int i = 0; i < N; i++) {
                    k = maxIndex(digitArray[i], N);
                    if (digitArray[i][indexJ] > max) {
                        indexI = i;
                        indexJ = k;
                        max = digitArray[indexI][indexJ];
                    }
                }
                break;
            }
            case TASK4: {
                int k = N - 1;
                for (int i = 0; i < N; i++) {
                    sum += digitArray[i][k--];
                }
                break;
            }
            case TASK5: {
                int k = -1;
                printf("\nPlease enter the number of column -> ");
                k = getInt();
                
                for (int i = 0; i < N; i++)
                    sum += digitArray[i][k - 1];
                break;
            }
            case TASK6: {
                int max = digitArray[0][0];
                int maxJ = 0;
                indexMaxI = 0;
                indexMaxJ = 0;
            
                for (int i = 0; i < N; i++) {
                    maxJ = maxIndex(digitArray[i], N);
                    if (digitArray[i][maxJ] > max) {
                        indexMaxI = i;
                        indexMaxJ = maxJ;
                        max = digitArray[indexMaxI][indexMaxJ];
                    }
                }
            
                int min = digitArray[0][0];
                int minJ = 0;
                indexMinI = 0;
                indexMinJ = 0;
            
                for (int i = 0; i < N; i++) {
                    minJ = minIndex(digitArray[i], N);
                    if (digitArray[i][minJ] < min) {
                        indexMinI = i;
                        indexMinJ = minJ;
                        min = digitArray[indexMinI][indexMinJ];
                    }
                }
            
                int tmp = digitArray[indexMinI][indexMinJ];
                digitArray[indexMinI][indexMinJ] = digitArray[indexMaxI][indexMaxJ];
                digitArray[indexMaxI][indexMaxJ] = tmp;
                break;
            }
            case TASK7: {
                int number = 0;
                r = 0;
                c = 0;
                printf("Please enter the number -> ");
                number = getInt();
                printf("Please enter the row of changable number -> ");
                r = getInt();
                printf("Please enter the column of changable number -> ");
                c = getInt();

                if(r <= N && c <= N) {
                    digitArray[r - 1][c - 1] = number;
                } else error = 1;
                break;
            }
            default:
                break;
        }
        Console_clear();
    } while (menu != QUIT);
}

void task3(int N) {
    char string[N];
    cleanArray(string);
    char line[] = "\n--------------------------------------------------------------------\n";
    int menu = -1;
    int error = 0;
    char symbol = '\0';
    char buffer[N];
    int minEnterIndex = 0;
    int minLength = 0;
    int composition = 1;

    cleanArray(buffer);
    do {
        Console_clear();
        if(menu != HELP && error != 1) {
            puts(string);
        }

        if (error == 1 && menu == TASK1) {
            printf("You make a mistake -- you enter more symbols than you can");
            error = 0;
        } if (menu == TASK3 && error == 0) {
            puts(buffer);
            cleanArray(buffer);
        } else if (menu == TASK3 && error == 1) {
            Console_setCursorAttribute(BG_INTENSITY_RED);
            printf("You go beyond the string!");
            Console_setCursorAttribute(BG_DEFAULT);
            error = 0;
        } else if (menu == TASK4) {
            if (symbol != '\0') {
                for (int i = 0; i < strlen(string); i++) {
                    if (string[i] != symbol) {
                        printf("%c", string[i]);
                    } else printf("%c", '\n');
                }
            }
        } else if (menu == TASK5) {
            puts(buffer);
            cleanArray(buffer);
        } else if (menu == HELP) {
            Console_clear();
            printf("If this appear, it means that you need a help.\n");
            printf("It is a list of commands:\n");
            printf("help -- show list of commands\n");
            printf("quit -- close 'task2'");
        } else if (menu == TASK6) {
            puts(buffer);
            cleanArray(buffer);
        } else if (menu == TASK7) {
            printf("Composition of numbers in string -> %i\n", composition);
        }

        puts(line);
        
        printf("List of task: \n"
        "1. Fill string\n"
        "2. Clear string\n"
        "3. Print segment of string\n"
        "4. Divide by substring with a symbol\n"
        "5. The shortest word\n"
        "6. Print all fractional numbers\n"
        "7. Composition of numbers\n\n");
        printf("Enter command, or number of task -> ");
        menu = getMenuCommand();
        switch (menu) {
            case TASK1:
                cleanArray(string);
                printf("Enter the string -> ");
                if (fgets(string, N + 2, stdin) != NULL)
                    break;
                else {
                    error = 1;
                    break;
                }
            case TASK2:
                cleanArray(string);
                break;
            case TASK3: {
                cleanArray(buffer);
                int length = 0;
                int start = 0;

                printf("Please, enter the number of symbol(not index!), from what you want to print -> ");
                start = getInt();
                if (start < 1) {
                    error = 1;
                    break;
                }
                printf("Please, enter the length of copied string -> ");
                length = getInt();
                if (length < 1) {
                    error = 1;
                    break;
                } 

                if (start  < strlen(string) - length) {
                    printSegment(buffer, string, start - 1, length);
                } else error = 1;

                break;
            }
            case TASK4: {
                symbol = '\0';
                printf("Please, enter the symbol -> ");
                symbol = getChar();
                break;
            }
            case TASK5: {
                int length = 0;
                int enterIndex = 0;

                for (int i = 0; i < strlen(string); i++) {
                    length = 0;
                    enterIndex = i;
                    while (isalpha(string[i]) != 0) {
                        length++;
                        i++;
                    }


                    if (minLength == 0 && minEnterIndex == 0) {
                        minLength = length;
                        minEnterIndex = enterIndex;
                    }

                    if (minLength >= length) {
                        cleanArray(buffer);
                        minLength = length;
                        minEnterIndex = enterIndex;
                        for(int j = 0; j < minLength; j++) {
                            buffer[j] = string[enterIndex + j];
                        }
                        buffer[minLength] = '\0';
                    }
                }
                break;
            }
            case TASK6: {
                int pointIndex = 0;
                int k = 0;
                int length = 0;
                
                for (int i = 0; i < strlen(string); i++) {
                    k = 0;
                    if (string[i] == '.' || string[i] == ',') {
                        if (isdigit(string[i - 1]) != 0 && isdigit(string[i + 1]) != 0) {
                            pointIndex = i;
                            while (isalpha(string[i]) == 0  && isspace(string[i]) == 0) {
                                i--;
                                buffer[k + length] = string[i];
                                k++;
                            }
                            //printf("%c", buffer[k]);

                            if (k > 1) {
                                for (int j = 0; j < (k / 2); j++) {
                                    int tmp = buffer[j + length];
                                    buffer[j + length] = buffer[k - j - 1 + length];
                                    buffer[k - j - 1 + length] = tmp;
                                }
                            }

                            i = pointIndex;
                            buffer[k + length] = '.';
                            k++;
                            while (isalpha(string[i]) == 0 && isspace(string[i]) == 0) {
                                i++;
                                buffer[k + length] = string[i];
                               // printf("%c", buffer[k]);
                                k++;
                            }
                        }
                    }
                    length += k;
                    //k = 0;
                }

                buffer[length] = '\0';
                break;
            }
            case TASK7: {
                composition = 1;
                int number = 0;
                int k = 0;

                for (int i = 0; i < strlen(string); i++) {
                    k = 0;
                    number = 1;
                    cleanArray(buffer);
                    if (isdigit(string[i]) && isspace(string[i - 1])) {
                        while (isdigit(string[i])) {
                            buffer[k] = string[i];
                            k++;
                            i++;
                        }
                        if(isspace(string[i])) {
                            buffer[k] = '\0';
                            number = atoi(buffer);
                            composition *= number;
                        }
                    }   
                }
            }
            default:
                break;
        }
        //Console_clear();
    } while (menu != QUIT);
}

void cleanArray(char *string) {
    for (int i = strlen(string) - 1; i >= 0; i--) {
        string[i] = '\0';
    }
}

void printSegment(char * buffer,char * string, int start, int length) {
        for (int i = 0; i < length; i++) {
            buffer[i] = string[start + i];
            if (i + 1 <= length) {
                buffer[i + 1] = '\0';
            }
        }
}

void randArray(int* digitArray, int size, int L, int H) {
    for (int i = 0; i < size; i++) {
        digitArray[i] = rand() % (H - L + 1) +  L;
    }
}

enum menuCommands getMenuCommand() {
    char point[6];
    fgets(point, 6, stdin);
    
    // for (int i = 0; i < strlen(point); i++)
    //     tolower(point[i]);
    
    if (strlen(point) > 1) {
        if (strncmp(point, "1", strlen(point) - 1) == 0)
            return TASK1;
        else if (strncmp(point, "2", strlen(point) - 1) == 0)
            return TASK2;
        else if (strncmp(point, "3", strlen(point) - 1) == 0)
            return TASK3;
        else if (strncmp(point, "4", strlen(point) - 1) == 0)
            return TASK4;
        else if (strncmp(point, "5", strlen(point) - 1) == 0)
            return TASK5;
        else if (strncmp(point, "6", strlen(point) - 1) == 0)
            return TASK6;
        else if (strncmp(point, "7", strlen(point) - 1) == 0)
            return TASK7;
        else if (strlen(point) == 5 && strncmp(point, "help", strlen(point) - 1) == 0)
            return HELP;
        else if (strlen(point) == 5 && strncmp(point, "quit", strlen(point) - 1) == 0)
            return QUIT;
    }
    return HELP;
}

int minIndex(int *digitArray, int N) {
    int min = digitArray[0];
    int index = 0;
    
    for (int i = 0; i < N; i++) {
        if (min > digitArray[i]) {
            min = digitArray[i];
            index = i;
        }
    }

    return index;
}

int sumArray(int *digitArray, int N) {
    int sum = 0;
    
    for (int i = 0; i < N; i++)
        sum += digitArray[i];

    return sum;
}

long multiplyArray(int *digitArray, int N) {
    long multiply = 1;
    
    for (int i = 0; i < N; i++) {
        if (digitArray[i] < 0)
            multiply *= digitArray[i];
    }

    if (multiply == 1)
        return 9173;

    return multiply;
}

void swap(int *digitArray, int N) {
    int minInd = minIndex(digitArray, N);
    int maxInd = maxIndex(digitArray, N);

    int tmp = digitArray[minInd];
    digitArray[minInd] = digitArray[maxInd];
    digitArray[maxInd] = tmp;
}

int maxIndex(int *digitArray, int N) {
    int max = digitArray[0];
    int maxIndex = 0;

    for (int i = 0; i < N; i++) {
        if (digitArray[i] > max) {
            max = digitArray[i];
            maxIndex = i;
        }
    }
    return maxIndex;
}

void multiplyElement(int *digitArray, int N, int number) {
    for (int i = 0; i < N; i++)
        digitArray[i] *= number;
}