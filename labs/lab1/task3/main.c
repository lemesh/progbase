#include <stdio.h>
#include <progbase.h>
#include <math.h>

int main(void) {
    int n = 0;
    int m = 0;
    float sum1 = 0;
    float sum2 = 0;

    printf("%s", "Enter 'n' -> ");
    scanf("%i", &n);
    puts("");

    printf("%s", "Enter 'm' -> ");
    scanf("%i", &m);
    puts("");

    printf("%s %i", "n = ", n);
    puts("");

    printf("%s %i", "m = ", m);
    puts("");

    

    if (n > 0 && m > 0) {
        printf("%s", "--------------------------------------------------------");
        puts("");

        for (int i = 1; i < n; i++) {
            sum2 = 0;
            for (int j = 1; j < m; j++) {
                sum2 += (2 / ((float)i + (float)j) + i * (j - 1));
                printf("%s %i %s %i %s %f %s %f", "i = ", i, " j = ", j, ": sum1 = ", sum1, " sum2 = ", sum2);
                puts("");
            }
            sum1 += sum2;
            
        }

        printf("%s", "--------------------------------------------------------");
        puts("");

        printf("%s %f", "x = ", sum1);
        puts("");
    } else {
        printf("Error! Incorrect input!");
    }
    
    return 0;
}