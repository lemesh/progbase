#include <stdio.h>
#include <progbase.h>
#include <math.h>

int main(void) {
    float x = 0.0;
    float y = 0.0;
    float z = 0.0;
    float a, a0, a1, a2;

    printf("%s", "Enter 'x' -> ");
    scanf("%f", &x);
    puts("");

    printf("%s", "Enter 'y' -> ");  
    scanf("%f", &y);
    puts("");

    printf("%s", "Enter 'z' -> ");
    scanf("%f", &z);
    puts("");

    printf("%s %f %s %s %f %s %s %f", " x -> ", x, "\n", "y -> ", y, "\n", "z -> ", z);
    puts("");

    if (z == 0 || (x - y) <= 0) {
        printf("%s", "a0 Can't be computed");
        puts("");
    } else {
        a0 = (pow(x, y + 1)) / (pow(x - y, 1 / z));
        printf("%s %f", "a0 = ", a0);
        puts("");
    }

    if (x + y == 0) {
        printf("%s", "a1 Can't be computed");
        puts("");
    } else {
        a1 = y / (2 * abs(x + y));
        printf("%s %f", "a1 = ", a1);
        puts("");
    }

    if (sin(x) == 0 || abs((cos(y) / sin(x)) + 1) < 0) {
        printf("%s", "a2 Can't be computed");
        puts("");
    } else {
        a2 = sqrt(abs((cos(y) / sin(x)) + 1));
        printf("%s %f", "a2 = ", a2);
        puts("");
    }

    if((z == 0 || (x - y) <= 0) || x + y == 0 || sin(x) == 0 || abs((cos(y) / sin(x)) + 1) < 0) {
        printf("%s", "a Can't be computed");
        puts("");
    } else {
        a = a0 + a1 + a2;
        printf("%s %f", "a = ", a);
        puts("");
    }

    return 0;
}