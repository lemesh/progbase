#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>
#include <progbase/console.h>

#define __LEN(A) sizeof(A)/sizeof(A[0])
#define TEST_ON_CONSONANTS "AEIOUYaeiouy"

enum menu {
    TASK1,
    TASK2,
    TASK3,
    TASK4,
    ERROR
};

struct Sentence {
    int index;
    char sentence[256];
};

struct SentenceArray {
    int size;
    struct Sentence *sentences;
};

int indexOfSentence(int, char *);
char *copySentence(char *, char *, int, int);
bool structEquals(struct Sentence, struct Sentence);
bool structArrayEquals(struct SentenceArray, struct SentenceArray);
void createSomeSentence(struct Sentence *,char *, int);
struct SentenceArray * getAllSentencesArrayNew(char *);
int sentenceLength(char *);
void arrayFree(struct SentenceArray *);
void printArray(struct SentenceArray *);
enum menu getMenuCommand(char**);

int fileExists(const char *);
long getFileSize(const char *);
int readFileToBuffer(const char *, char *, int);

int main(int argc, char * argv[]) {
    if (argc == 1) return EXIT_FAILURE;
    int menu = getMenuCommand(argv);

    switch (menu) {
        case TASK1: {
            int N = 256;
            char buffer[N];
            if (argc > 2) return EXIT_FAILURE;
            //task 1.1

            assert(indexOfSentence(2, "Hello. How are you?") == 7);
            assert(indexOfSentence(2, "Hello.") == -1);
            assert(indexOfSentence(2, "Hello.!?..!? How are you?") == 13);
            assert(indexOfSentence(2, "Hello.!?..!?  How are you?."
            " What do you think about this?") == 14);
            assert(indexOfSentence(2, "") == -1);

            //task 1.2

            assert(strcmp(copySentence("Hello. How are you?", buffer, N, 1), "Hello.") == 0);
            assert(strcmp(copySentence("Hello. Hw are you?", buffer, N, 2), "Hw are you?") == 0);
            assert(strcmp(copySentence("Hello. How are you?", buffer, N, 1), "ello") != 0);
            assert(strcmp(copySentence(". How are you?", buffer, N, 1), ".") == 0);
            assert(strcmp(copySentence("", buffer, N, 1), "") == 0);
            assert(strcmp(copySentence("", buffer, N, 1), "Hello") != 0);
            assert(strcmp(copySentence("Hello, my name is Oleg."
            " What do you think, about this.", buffer, N, 1), "Hello, my name is Oleg") != 0);
            assert(strcmp(copySentence("Hello, my name is Oleg"
            " What do you think, about this Hello, my name is Oleg"
            " What do you think, about this Hello, my name is Oleg"
            " What do you think, about this Hello, my name is Oleg"
            " What do you think, about this Hello, my name is Oleg"
            " What do you think, about this Hello, my name is Oleg"
            " What do you think, about this", buffer, N, 1), "") == 0);
            break;
        }
        case TASK2: {
            if (argc > 2) return EXIT_FAILURE;
            //task 2.1

            struct Sentence sentence1;
            createSomeSentence(&sentence1, "Hello. How are you?", 2);
            struct Sentence sentence2 ;
            createSomeSentence(&sentence2, "Hello. Hw are you?", 2);
            
            assert(structEquals(sentence1,sentence2) == false);

            createSomeSentence(&sentence1, "Hello. How are you?", 2);
            createSomeSentence(&sentence2,"Hello. How are you?", 2);

            assert(structEquals(sentence1,sentence2) == true);

            createSomeSentence(&sentence1, "Hello. How are you?", 1);
            createSomeSentence(&sentence2,"Hello. How are you?", 2);

            assert(structEquals(sentence1,sentence2) == false);

            //task 2.2

            struct Sentence sentences[2] = {
                {2, "Hello. How are you?"},
                {2, "Hello. How are you?"}
            };
            
            struct SentenceArray array1 = {
                .size = 2,
                .sentences = sentences
            };

            struct SentenceArray array2 = {
                .size = 2,
                .sentences = sentences
            };

            assert(structArrayEquals(array1,array2) == true);

            //-----------------------------------------------------------------------------------------

            struct Sentence sentences1[2] = {
                {2, "Hello. How are you?"},
                {2, "Hello. How are you?"}
            };
            
            struct SentenceArray array = {
                .size = 2,
                .sentences = sentences1
            };

            struct Sentence sentences2[2] = {
                {2, "Hello. How are you?"},
                {2, "Hello. Hw are you?"}
            };

            struct SentenceArray array3 = {
                .size = 2,
                .sentences = sentences2
            };

            assert(structArrayEquals(array,array3) == false);

            //-----------------------------------------------------------------------------------------

            struct Sentence sentences3[2] = {
                {1, "Hello. How are you?"},
                {2, "Hello. How are you?"}
            };
            
            struct SentenceArray array4 = {
                .size = 2,
                .sentences = sentences3
            };

            struct Sentence sentences4[2] = {
                {2, "Hello. How are you?"},
                {2, "Hello. Hw are you?"}
            };

            struct SentenceArray array5 = {
                .size = 2,
                .sentences = sentences4
            };

            assert(structArrayEquals(array4,array5) == false);

            //task 2.3

            struct SentenceArray arraySent;

            struct Sentence new[2];
            createSomeSentence(&new[0], "Hello. How are you?", 2);
            createSomeSentence(&new[1], "Hello. How are you?", 2);

            arraySent.size = 2;
            arraySent.sentences = new;

            struct SentenceArray arraySent2;

            struct Sentence new2[2];
            createSomeSentence(&new2[0], "Hello. How are you?", 2);
            createSomeSentence(&new2[1], "Hello. How are you?", 2);

            arraySent2.size = 2;
            arraySent2.sentences = new2;

            assert(structArrayEquals(arraySent, arraySent2) == true);

            struct SentenceArray arraySent3;

            struct Sentence new3[2];
            createSomeSentence(&new3[0], "Hello. How are you?", 2);
            createSomeSentence(&new3[1], "Hello. How are you?", 2);

            arraySent3.size = 2;
            arraySent3.sentences = new3;

            struct SentenceArray arraySent4;

            struct Sentence new4[2];
            createSomeSentence(&new4[0], "Hello. Hw are you?", 2);
            createSomeSentence(&new4[1], "Hello. Hw are you?", 2);

            arraySent4.size = 2;
            arraySent4.sentences = new4;

            assert(structArrayEquals(arraySent3, arraySent4) == false);

            struct SentenceArray arraySent5;

            struct Sentence new5[2];
            createSomeSentence(&new5[0], "Hello. How are you?", 2);
            createSomeSentence(&new5[1], "Hello. How are you?", 2);

            arraySent5.size = 2;
            arraySent5.sentences = new5;

            struct SentenceArray arraySent6;

            struct Sentence new6[2];
            createSomeSentence(&new6[0], "Hello. Hw are you?", 2);
            createSomeSentence(&new6[1], "Hello. Hw are you?", 2);

            arraySent6.size = 2;
            arraySent6.sentences = new6;

            assert(structArrayEquals(arraySent5, arraySent6) == false);
            break;
        }
        case TASK3: {
            if (argc > 2) return EXIT_FAILURE;
             //task 3
            struct SentenceArray * new;
            new = getAllSentencesArrayNew("Hello. How are you? My name is Oleh");

            struct Sentence sentences[3] = {
                {
                    .sentence = "Hello.",
                    .index = 0
                },
                {
                    .sentence = "How are you?",
                    .index = 7
                },
                {
                    .sentence = "My name is Oleh",
                    .index = 20
                }
            };

            struct SentenceArray new1 = {
                .sentences = sentences,
                .size = 3
            };

            assert(structArrayEquals(*new,new1) == true);

            arrayFree(new);

            new = getAllSentencesArrayNew("Hello. How are you?");

            struct Sentence sentences1[2] = {
                {
                    .sentence = "Hello.",
                    .index = 0
                },
                {
                    .sentence = "How are you?",
                    .index = 7
                }
            };

            new1.sentences = sentences1;
            new1.size = 2;

            assert(structArrayEquals(*new,new1) == true);

            arrayFree(new);

            new = getAllSentencesArrayNew("Hello.");

            struct Sentence sentences2[1] = {
                {
                    .sentence = "Hello.",
                    .index = 0
                }
            };

            new1.sentences = sentences2;
            new1.size = 1;

            assert(structArrayEquals(*new,new1) == true);

            arrayFree(new);
            break;
        }
        case TASK4:
            if (argc == 2) return EXIT_FAILURE;
            if(fileExists(argv[2]) == 1) {
                char * filename = argv[2];
                long fileSize = getFileSize(filename);
                char * buffer;
                long bufferLen = fileSize + 1;
                if (fileSize != -1 && fileSize != 0) {
                    buffer = malloc(fileSize + 1);
                    long result = readFileToBuffer(filename, buffer, bufferLen);
                    if (result == -1) {
                        free(buffer);
                        return EXIT_FAILURE;
                    } 
                    buffer[fileSize] = '\0';
                    int length = 0;
                    char bufSentence[256];
                    int i = 1;
                    do {
                        memset(bufSentence, '\0', 100); 
                        copySentence(buffer, bufSentence, 256, i);
                        length = sentenceLength(bufSentence);
                        if (length > 0 && length < 10) {
                            Console_setCursorAttribute(BG_INTENSITY_RED);
                            puts(bufSentence);
                            Console_setCursorAttribute(BG_DEFAULT);
                        } else if (length < 25) {
                            Console_setCursorAttribute(BG_INTENSITY_YELLOW);
                            puts(bufSentence);
                            Console_setCursorAttribute(BG_DEFAULT);
                        } else {
                            Console_setCursorAttribute(BG_INTENSITY_GREEN);
                            puts(bufSentence);
                            Console_setCursorAttribute(BG_DEFAULT);
                        }
                        i++;
                    } while (strcmp(bufSentence, ""));

                    puts("");
                    i = 1;
                    length = 0;

                    do {
                        memset(bufSentence, '\0', 100); 
                        copySentence(buffer, bufSentence, 256, i);
                        length = sentenceLength(bufSentence);
                        if (length > 10) {
                            int status = 0;
                            for (int j = 0; j < 13; j++) {
                                if(bufSentence[0] != TEST_ON_CONSONANTS[j]) {
                                    status = 1;
                                } else {
                                    status = 0;
                                    break;
                                }
                            }
                            if (status == 1) {
                                puts(bufSentence);
                            }
                        }
                        i++;
                    } while (strcmp(bufSentence, ""));
                    free(buffer);
                } else return EXIT_FAILURE;
            } else return EXIT_FAILURE;
            break;
        default:
            return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void createSomeSentence(struct Sentence * new, char * string, int index) {
    char buffer[256];
    new->index = indexOfSentence(index, string);
    copySentence(string, buffer, 256, index);
    memset(new->sentence, '\0', 256);
    strcpy(new->sentence, buffer);

}

bool structArrayEquals(struct SentenceArray array1, struct SentenceArray array2) {
    if (array1.size == array2.size) {
        for (int i = 0; i < array1.size; i++) {
            if(structEquals(array1.sentences[i], array2.sentences[i]) == false)
                return false;
        }
        return true;
    }
    return false;
}

bool structEquals(struct Sentence sentence1, struct Sentence sentence2) {
    if (sentence1.index == sentence2.index) {
        if (!strcmp(sentence1.sentence, sentence2.sentence))
            return true;
    }
    return false;
}

int indexOfSentence(int sentence, char * string)    {
    if (strlen(string) < 1 || sentence < 1)
        return -1;

    int indexOfSentence = 0;
    int i = 0;
    int sentenceNumber = 1;

    while (sentenceNumber != sentence && string[i] != '\0') {
        while (string[i] != '.' && string[i] != '!' && string[i] != '?' 
        && string[i] != '\0' && string[i] != '\n') {
            indexOfSentence++;
            i++;
        }

        while (string[i] == '.' || string[i] == '!' || string[i] == '?' 
        || string[i] == ' ' || string[i] == '\n') {
            indexOfSentence++;
            i++;
        }

        if(isalpha(string[i]) || isdigit(string[i]))
            sentenceNumber++;
    }

    if(sentenceNumber < sentence) {
        return -1;
    }
    return indexOfSentence;
}

char *copySentence(char *string, char *buffer, int buffLength, int sentence) {
    memset(buffer, '\0', buffLength);

    int i = indexOfSentence(sentence, string);
    
    if (i == -1)
        return "";

    if (strlen(string) < 1 || i >= strlen(string) || i == -1)
        return "";

    int k = 0;
    
    while(string[i] != '.' && string[i] != '!' && string[i] != '?'
     && string[i] != '\0' && string[i] != '\n') {
        buffer[k] = string[i];
        i++;
        k++;
    }

    while((string[i] == '.' || string[i] == '!' || string[i] == '?') && string[i] != '\0') {
        buffer[k] = string[i];
        i++;
        k++;
    }

    if (k < buffLength)
        return buffer;
    else
        return "";
}

struct SentenceArray * getAllSentencesArrayNew(char * str) {
    int count = 0;
    int i = 0;
    char buffer[256];

    struct Sentence * newSentence = NULL;

    copySentence(str, buffer, 256, 1);
    
    while (strcmp(buffer, "") != 0) {
        count++;
        newSentence = realloc(newSentence, count * sizeof(struct Sentence));
        createSomeSentence(&newSentence[i], str, i + 1);
        i++;
        memset(buffer, '\0', 256);
        copySentence(str, buffer, 256, i + 1);
    }

    struct SentenceArray * new = malloc(sizeof(struct SentenceArray));
    new->size = i;
    new->sentences = newSentence;

    return new;
}

void arrayFree(struct SentenceArray * array) {
    free(array->sentences);
    free(array);
}

void printArray(struct SentenceArray * new) {
    printf("Size -> %i\n", new->size);
    for (int i = 0; i < new->size; i++) {
        puts(new->sentences[i].sentence);
    }
}

int fileExists(const char *fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // false: not exists
    fclose(f);
    return 1;  // true: exists
}

long getFileSize(const char *fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return -1;  // error opening file
    fseek(f, 0, SEEK_END);  // rewind cursor to the end of file
    long fsize = ftell(f);  // get file size in bytes
    fclose(f);
    return fsize;
}

int readFileToBuffer(const char *fileName, char *buffer, int bufferLength) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // read 0 bytes from file
    long readBytes = fread(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes;  // number of bytes read
}

enum menu getMenuCommand(char * task[]) {
    if (!strcmp(task[1], "task1")) {
        return TASK1;
    }
    if (!strcmp(task[1], "task2")) {
        return TASK2;
    } 
    if (!strcmp(task[1], "task3")) {
        return TASK3;
    } 
    if (!strcmp(task[1], "task4")) {
        return TASK4;
    } 
    return ERROR;
}

int sentenceLength(char * buff) {
    int length = 0;
    while (buff[length] != '\0') {
        length++;
    }

    if (length <= 0) return -1;
    return length;
}