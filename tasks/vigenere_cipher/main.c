#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

bool fileExists(const char *filename);
long getFileSize(const char *filename);
int readFileToBuffer(const char *filename,char *buffer, int bufferLength);
int writeBufferToFile(const char *filename,char *buffer, int bufferLength);
void encryption(char *buffer, char *key, int *textLength, char *encryptText, int keyLength);
void decryption(char *encryptText, char *key, char *decryptText, int textLength);
void deleteSpaces(char *text, int *length);
void rightKey(char *key, int textLength, int keyLength);

int main(int argc, char * argv[]) {
    if (argc == 1) return EXIT_FAILURE;

    char *filename = NULL;
    char *outputFile = NULL;
    char *buffer = NULL;
    char *encryptText = NULL;
    char *decryptText = NULL;
    int outputSize = -1;
    int size = -1;
    char *key = NULL;
    bool decrypt = false;
    bool output = false;

    for (int i = 1; i < argc; i++) {
        if(!strcmp(argv[i], "-f")) {
            if (argv[i + 1] != NULL && strcmp(argv[i + 1],"-o")
            && strcmp(argv[i + 1], "-k") && strcmp(argv[i + 1], "-d")
            && strcmp(argv[i + 1], "-f")) {
                filename = argv[i + 1];
                if(!fileExists(filename)) return EXIT_FAILURE;
                size = getFileSize(filename);
                if(size == -1) return EXIT_FAILURE;
                buffer = malloc(size);
                readFileToBuffer(filename, buffer, size);
            }
        } else if(!strcmp(argv[i], "-k")) {
            if (argv[i + 1] != NULL && strcmp(argv[i + 1],"-o")
            && strcmp(argv[i + 1], "-k") && strcmp(argv[i + 1], "-d")
            && strcmp(argv[i + 1], "-f")) {
                
                key = malloc(strlen(argv[i+1]));
                strcpy(key, argv[i + 1]);
            
            } else return EXIT_FAILURE;

        } else if(!strcmp(argv[i], "-o")) {
            if (argv[i + 1] != NULL && strcmp(argv[i + 1],"-o") != 0
            && strcmp(argv[i + 1], "-k") != 0 && strcmp(argv[i + 1], "-d") != 0
            && strcmp(argv[i + 1], "-f") != 0) {
                outputFile = argv[i + 1];
                if(!fileExists(outputFile)) return EXIT_FAILURE;
                output = true;
            } else return EXIT_FAILURE;
        } else if(!strcmp(argv[i], "-d")) {
            decrypt = true;
        }
    }

    if(buffer == NULL) return EXIT_FAILURE;
    
    if (decrypt == true && key == NULL) return EXIT_FAILURE; 
    else if (decrypt == true) {
        decryptText = malloc(size);
        decryption(buffer, key, decryptText, size);
        if (output == true) {
            writeBufferToFile(outputFile, decryptText, size);
        } else {
            puts(key);
            puts(decryptText);
        }
    } else {
        encryptText = malloc(size);
        if (key != NULL) {
            int keyLength = strlen(key);
            encryption(buffer, key, &size, encryptText, keyLength);
            puts(encryptText);
        } else return EXIT_FAILURE;
    }

    free(buffer);
    free(encryptText);
    free(decryptText);
    free(key);
    return EXIT_SUCCESS;
}

bool fileExists(const char *filename) {
    FILE *f = fopen(filename, "rb");
    if (!f) return false;
    fclose(f);
    return true;
}

long getFileSize(const char *filename) {
    FILE *f = fopen(filename, "rb");
    if(!f) return -1;
    fseek(f, 0, SEEK_END);
    long size = ftell(f);
    fclose(f);
    return size;
}

int readFileToBuffer(const char *filename,char *buffer, int bufferLength) {
    FILE *f = fopen(filename, "rb");
    if (!f) return EXIT_FAILURE;
    long readBytes = fread(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes;
}

void encryption(char *buffer, char *key, int *textLength, char *encryptText, int keyLength) {
    if (*textLength > 1) {
        deleteSpaces(buffer, textLength);
        deleteSpaces(key,&keyLength);
        rightKey(key, *textLength, keyLength);

        int i = 0;
        for(;i < *textLength; i++) {
            if(isalpha(buffer[i]) && isalpha(key[i]))
                encryptText[i] = ((int)buffer[i] + (int)key[i] - 2*'a') % 26 + 'a';
        }
        encryptText[i] = '\0';
    }
}

void deleteSpaces(char *text,int *length) {
    int i = 0; 
    bool spaces = true;
    while (spaces == true) {
        i = 0;
        for (; i < *length; i++) {
            if (isspace(text[i])) {
                spaces = true;
                i++;
                for (; i < *length; i++) {
                    text[i - 1] = text[i];
                }
                text[*length - 1] = '\0';
            (*length)--;
            } else spaces = false;
        }
    }
    //text = realloc(text, *length);
}

void rightKey(char *key, int textLength, int keyLength) {
    int i = 0;
    for(;keyLength != textLength + 1;i++) {
        if (keyLength >= textLength) {
            keyLength--;
            key[keyLength - 1] = '\0';
            //key = realloc(key, keyLength);
        } else if (keyLength <= textLength) {
            //key = realloc(key, keyLength);
            key[keyLength - 1] = key[i];
            keyLength++;
            key[keyLength - 1] = '\0';
        }
     }
}

void decryption(char *encryptText, char *key, char *decryptText, int textLength) {
    if (key != NULL) {
        int keyLength = strlen(key);
        deleteSpaces(encryptText, &textLength);
        deleteSpaces(key, &keyLength);
        rightKey(key, textLength, keyLength);
        int i = 0;
        for(;i < textLength; i++) {
            if(isalpha(encryptText[i]) && isalpha(key[i]))
                decryptText[i] = abs((int)encryptText[i] - (int)key[i] + 26) % 26 + 'a';
            else decryptText[i] = encryptText[i];
        }
        decryptText[i] = '\0';
    }
}

int writeBufferToFile(const char *filename,char *buffer, int bufferLength) {
    FILE *f = fopen(filename, "w+");
    if (!f) return EXIT_FAILURE;
    long readBytes = fwrite(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes;
}

/*  abcdefghijklmnoprstuvwxyz
    bcdefghijklmnoprstuvwxyza
    cdefghijklmnoprstuvwxyzab
    defghijklmnoprstuvwxyzabc
    efghijklmnoprstuvwxyzabcd
    fghijklmnoprstuvwxyzabcde
    ghijklmnoprstuvwxyzabcdef
    hijklmnoprstuvwxyzabcdefg
    ijklmnoprstuvwxyzabcdefgh
    jklmnoprstuvwxyzabcdefghi
    klmnoprstuvwxyzabcdefghij
    lmnoprstuvwxyzabcdefghijk
    mnoprstuvwxyzabcdefghijkl
    noprstuvwxyzabcdefghijklm
    oprstuvwxyzabcdefghijklmn
    prstuvwxyzabcdefghijklmno
    rstuvwxyzabcdefghijklmnop
    stuvwxyzabcdefghijklmnopr
    tuvwxyzabcdefghijklmnoprs
    uvwxyzabcdefghijklmnoprst
    vwxyzabcdefghijklmnoprstu
    wxyzabcdefghijklmnoprstuv
    xyzabcdefghijklmnoprstuvw
    yzabcdefghijklmnoprstuvwx
    zabcdefghijklmnoprstuvwxy */