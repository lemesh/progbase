#include <stdio.h>
#include <math.h>
#include <progbase.h>

#define PI 3.14159

int main() {
    for(float x = -10.0; x<=10; x+=0.5) {
    float function1 = 2 * sin(0.5 * x);
    float function2 = 0.4 * (cos(x)/sin(x)) - 2;
    float sum = function1 + function2;
    float multiply = function1 * function2;
    float divide1 = function1/function2;
    float divide2 = function2/function1;

    printf("%s %f","x = ", x);
    puts("");
    printf("%s %f","F1(x) = ", function1);
    puts("");
    if(x == 0) {
        printf("%s","F2(x) Can't be computed!");
        puts("");
    } else if((cos(x)/sin(x)) == PI) {
        printf("%s","F2(x) Can't be computed!");
        puts("");
        break;
    } else {
        printf("%s %f","F2(x) = ", function2);
        puts("");
        printf("%s %f","F1(x) + F2(x) = ",sum);
        puts("");
        printf("%s %f","F1(x) * F2(x) = ",multiply);
        puts("");
        if(function2 == 0) {
            printf("%s","F1(x) / F2(x) Division by zero");
            puts("");
        } else {
            printf("%s %f","F1(x) / F2(x) = ",divide1);
            puts("");
        }
    
        if(function1==0) {
            printf("%s","F2(x) / F1(x) Division by zero");
            puts("");
        } else {
             printf("%s %f","F2(x) / F1(x) = ",divide2);
             puts("");
        }
    }

    printf("%s","--------------------------------------------------");
    puts("");
}



    return 0;
}