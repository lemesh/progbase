#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>

int sumFirstLast(const char * const str, const int lastCh);

int main() {
    char str[100];
    int sum = 0;
    int lastChar = 0;
    printf("Enter a string -> ");
    fgets(str, 100, stdin);
    str[strlen(str) - 1] = '\0';
    printf("Entered string -> %s\n", str);
    sum = sumFirstLast(str, strlen(str) - 1);
    printf("\nSum of first and last digit in string: %i\n", sum);
    
    assert(sumFirstLast("fdsds 32 dsfds 2 fd 2", strlen("fdsds 32 dsfds 2 fd 2") - 1) 
    == 5);
    assert(sumFirstLast("2fdsds 32 dsfds 7 fd", strlen("2fdsds 32 dsfds 7 fd") - 1) 
    == 9);
    assert(sumFirstLast("", -1) 
    == 1);
    assert(sumFirstLast("", 0) 
    == 1);
    assert(sumFirstLast("2fdsds", strlen("2fdsds") - 1) 
    == 2);
    return EXIT_SUCCESS;
}
    
int sumFirstLast(const char * const str, const int lastCh) {
    const char firstChar = str[0];
    const char lastChar = str[lastCh];
    if (lastCh < 0) return EXIT_FAILURE;
    if(firstChar == '\0') return EXIT_FAILURE;
    else if (!isdigit(firstChar)) return sumFirstLast(str + 1, lastCh - 1);
    else if (!isdigit(lastChar)) return sumFirstLast(str, lastCh - 1);
    else {
        if(lastCh == 0) return firstChar - '0';
        const int sum = firstChar - '0' + lastChar - '0';
        return sum;
    }
}