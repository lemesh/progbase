#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>

int main(void) {
    Console_clear();

    for(int x = 0; x < 16; x++) {
        for(int y =0; y < 28; y++) {
            if((x == 1 && (y < 12 || y > 15)) 
            || (x == 0)
            || (x == 10 && (y == 11 || y == 15))
            || (x == 11 && (y > 11 && y < 15))) {
                Console_setCursorAttribute(BG_INTENSITY_RED);
                printf("%c", ' ');
            } else if((x == 1 && (y > 11 && y < 16))
            || (x == 2 && (y > 9 && y < 18))
            || (x == 3 && (y > 8 && y < 19))
            || (x == 4 && ((y > 7 && y < 10) || (y > 11 && y < 16) || (y > 17 && y < 20)))
            || (x == 5 && ((y > 7 && y < 10) || (y > 11 && y < 16) || (y > 17 && y < 20)))
            || (x == 6 && (y > 6 && y < 21))
            || (x == 7 && (y > 6 && y < 21))
            || (x == 8 && ((y > 6 && y < 12) || y == 13 || (y > 14 && y < 21)))
            || (x == 9 && (y > 7 && y < 20))
            || (x == 10 && ((y > 7 && y < 11) || (y > 11 && y < 15) || (y > 15 && y < 20)))
            || (x == 11 && ((y > 8 && y < 12) || (y > 14 && y < 19)))
            || (x == 12 && (y > 9 && y < 18))
            || (x == 13 && (y > 10 && y < 17))
            || (x == 14 && (y > 11 && y < 16))) {
                Console_setCursorAttribute(BG_WHITE);
                printf("%c", ' ');
            } else if((x == 2 && (y < 10 || y > 17))
            || (x == 3 && (y < 9 || y > 18))) {
                Console_setCursorAttribute(BG_YELLOW);
                printf("%c", ' ');
            } else if ((x == 4 && (y < 8 || y > 19))
            || (x == 5 && (y < 8 || y > 19))) {
                Console_setCursorAttribute(BG_INTENSITY_YELLOW);
                printf("%c", ' ');
            } else if ((x == 6 && (y < 7 || y > 20))
            || (x == 7 && (y < 7 || y > 20))) {
                Console_setCursorAttribute(BG_GREEN);
                printf("%c", ' ');
            } else if ((x == 8 && (y < 7 || y > 20))
            || (x == 9 && (y < 8 || y > 19))
            || (x == 10 && (y < 8 || y > 19))) {
                Console_setCursorAttribute(BG_CYAN);
                printf("%c", ' ');
            } else if ((x == 11 && (y < 9 || y > 18))
            || (x == 12 && (y < 10 || y > 17))) {
                Console_setCursorAttribute(BG_BLUE);
                printf("%c", ' ');
            } else if ((x == 13 && (y < 11 || y > 16))
            || (x == 14 && ( y < 12 || y > 15))
            || (x == 15)) {
                Console_setCursorAttribute(BG_MAGENTA);
                printf("%c", ' ');  
            } else if ((x == 4 && (y == 11 || y == 17))
            || (x == 5 && ((y > 9 && y < 12) || (y > 15 && y < 18)))
            || (x == 8 && (y == 12 || y == 14))) {
                Console_setCursorAttribute(BG_BLACK);
                printf("%c", ' ');
            } else if (x == 4 && (y == 10 || y == 16)) {
                Console_setCursorAttribute(BG_INTENSITY_WHITE);
                printf("%c", ' ');
            }
        }
        puts("");
    }

    return 0;
}