#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>

int main(void) {
    const int size = 10;
    srand(time(0));

    int A[size];

    printf("---------------------------------------------------------------\n");

    printf("1. To create Array\n");

    for (int i = 0; i < size; i++) {
        A[i] = rand() % 299 - 100;
        printf("%i %s", A[i] ," ");
    }
    
    puts("");

    for (int i = size - 1; i >= 0; i--) {
        printf("%i %s", A[i] ," ");
    }

    printf("\n---------------------------------------------------------------\n");

    printf("2. Show numbers, that greater, than 100 or less, than - 50\n");

    for (int i = 0; i < size; i++) {
        if(A[i] < -50 || A[i] > 100) {
            Console_setCursorAttribute(BG_INTENSITY_RED);
            printf("%i", A[i]);
            Console_setCursorAttribute(BG_DEFAULT);
            printf(" ");
        } else {
            printf("%i %s", A[i] ," ");
        }
    }

    printf("\n---------------------------------------------------------------\n");

    printf("3. Print numbers less, than 0\n");

    int count = 0;
    int sum = 0;

    for (int i = 0; i < size; i++) {
        if(A[i] < 0) {
            Console_setCursorAttribute(BG_INTENSITY_RED);
            printf("%i", A[i]);
            Console_setCursorAttribute(BG_DEFAULT);
            printf(" ");
            count ++;
            sum += A[i];
        } else {
            printf("%i %s", A[i] ," ");
        }
    }

    puts("");
    printf("%s %i", "Count = ", count);
    puts("");

    printf("%s %i", "Sum = ", sum);
    puts("");

    printf("%s %f", "Average = ", (float)sum / count);

    printf("\n---------------------------------------------------------------\n");

    printf("4. Print Max and Min number with it Index\n");

    int maxIndex = 0;
    int minIndex = 0;
    int max = -50;
    int min = A[0];


    for (int i = 0; i < size; i++) {
        if (A[i] < 50 && A[i] > -50) {
            if (A[i] > max) {
                max = A[i];
                maxIndex = i;
            } else if (A[i] < min) {
                min = A[i];
                minIndex = i;
            }
        } else {
            if(A[i] < min) {
                min = A[i];
                minIndex = i;
            }
        }
    }

    printf("%s %i %s %i", "MaxIndex = ", maxIndex, "\nMax = ", max);
    puts("");
    printf("%s %i %s %i", "MinIndex = ", minIndex, "\nMin = ", min);

    printf("\n---------------------------------------------------------------\n");

    printf("5. Convert 'int' to 'char'\n");

    char S[size];

    for (int i = 0; i < size; i++) {
        S[i] = (A[i] % 95) + 32;
    }

    S[size] = '\0';

    puts(S);

    printf("\n---------------------------------------------------------------\n");

    printf("6. Transform number from '-50' to '50' into '0'\n");

    for (int i = 0; i < size; i++) {
        if (A[i] > -50 && A[i] < 50) {
            Console_setCursorAttribute(BG_INTENSITY_RED);
            A[i] = 0;
            printf("%i", A[i]);
            Console_setCursorAttribute(BG_DEFAULT);
            printf(" ");
        } else {
            printf("%i %s", A[i], " ");
        }
    }

    printf("\n---------------------------------------------------------------\n");
    
    return 0;
}