#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>

int main(void)
{
    /* colors encoding table */
    int size = 28;
    int task = 0;
    char image[28][28] = {
        {'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'},
        {'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'},
        {'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'},
        {'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'},
        {'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'},
        {'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'},
        {'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F'},
        {'F', 'F', 'F', '2', '2', '2', '2', '2', '2', '2', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', '5', '5', '5', '5', '5', '5', '5', 'F', 'F', 'F'},
        {'F', 'F', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', 'F', 'F', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', 'F', 'F'},
        {'F', '3', '3', '3', '7', '7', '7', '0', '7', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', '7', '0', '7', '7', '7', '3', '3', '3', 'F'},
        {'3', '7', '3', '7', '7', '7', '7', '0', '7', '7', '7', 'F', 'F', 'F', 'F', 'F', 'F', '7', '7', '7', '0', '7', '7', '7', '7', '3', '7', '3'},
        {'3', '7', '3', '3', '7', '7', '7', '7', '0', '7', '7', '7', 'F', 'F', 'F', 'F', '7', '7', '7', '0', '7', '7', '7', '7', '3', '3', '7', '3'},
        {'F', '3', '7', '7', '7', '7', '7', '0', '0', '0', '0', 'F', 'F', 'F', 'F', 'F', 'F', '0', '0', '0', '0', '7', '7', '7', '7', '7', '3', 'F'},
        {'F', 'F', '7', '7', '7', '7', '7', '7', '7', '7', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', '7', '7', '7', '7', '7', '7', '7', '7', 'F', 'F'},
        {'F', 'F', '2', '2', '8', '2', '2', '8', '2', '2', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', '5', '5', 'A', '5', '5', 'A', '5', '5', 'F', 'F'},
        {'F', '2', '2', '2', '8', '2', '2', '8', '2', '2', '2', 'F', 'F', 'F', 'F', 'F', 'F', '5', '5', '5', 'A', '5', '5', 'A', '5', '5', '5', 'F'},
        {'2', '2', '2', '2', '8', '8', '8', '8', '2', '2', '2', '2', 'F', 'F', 'F', 'F', '5', '5', '5', '5', 'A', 'A', 'A', 'A', '5', '5', '5', '5'},
        {'7', '7', '2', '8', '6', '8', '8', '6', '8', '2', '7', '7', 'F', 'F', 'F', 'F', '7', '7', '5', 'A', 'C', 'A', 'A', 'C', 'A', '5', '7', '7'},
        {'7', '7', '7', '8', '8', '8', '8', '8', '8', '7', '7', '7', 'F', 'F', 'F', 'F', '7', '7', '7', 'A', 'A', 'A', 'A', 'A', 'A', '7', '7', '7'},
        {'7', '7', '8', '8', '8', '8', '8', '8', '8', 'F', '7', 'F', 'F', 'F', 'F', 'F', 'F', '7', 'F', 'A', 'A', 'A', 'A', 'A', 'A', 'A', '7', '7'},
        {'F', 'F', '8', '8', '8', 'F', 'F', 'F', '8', '8', '8', 'F', 'F', 'F', 'F', 'F', 'F', 'A', 'A', 'A', 'F', 'F', 'F', 'A', 'A', 'A', 'F', 'F'},
        {'A', 'A', 'A', 'A', 'A', 'F', 'F', 'F', 'A', 'A', 'A', 'A', 'A', 'F', 'F', 'B', 'B', 'B', 'B', 'B', 'F', 'F', 'F', 'B', 'B', 'B', 'B', 'B'},
        {'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'},
        {'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'},
        {'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'},
        {'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'},
        {'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'},
        {'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'}};

    const char colorsTable[16][2] = {
        {'0', BG_BLACK},
        {'1', BG_INTENSITY_BLACK},
        {'2', BG_RED},
        {'3', BG_INTENSITY_YELLOW},
        {'4', BG_DEFAULT},
        {'5', BG_INTENSITY_GREEN},
        {'6', BG_YELLOW},
        {'7', BG_WHITE},
        {'8', BG_INTENSITY_BLUE},
        {'9', BG_DEFAULT},
        {'A', BG_MAGENTA},
        {'B', BG_INTENSITY_MAGENTA},
        {'C', BG_INTENSITY_CYAN},
        {'D', BG_DEFAULT},
        {'E', BG_DEFAULT},
        {'F', BG_INTENSITY_WHITE}};
        int colorsTableLength = sizeof(colorsTable) / sizeof(colorsTable[0]);
        char colorsPalette[] = "0123456789ABCDEF";
        int colorsPaletteLength = sizeof(colorsPalette) / sizeof(colorsPalette[0]);
        int i = 0;
        int colorPairIndex = 0;

    printf("Enter a number of task -> ");
    scanf("%i", &task);
    puts("");

    switch (task)
    {
    case 1: {
        int colorsTableLength = sizeof(colorsTable) / sizeof(colorsTable[0]);
        char colorsPalette[] = "0123456789ABCDEF";
        int colorsPaletteLength = sizeof(colorsPalette) / sizeof(colorsPalette[0]);
        int i = 0;
        int colorPairIndex = 0;
        Console_clear();
        for (i = 0; i < colorsPaletteLength; i++)
        {
            char colorCode = '\0';
            char color = '\0';
            /* get current color code from colorsPalette */
            colorCode = colorsPalette[i];
            /* find corresponding color in table */
            for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++)
            {
                char colorPairCode = colorsTable[colorPairIndex][0];
                char colorPairColor = colorsTable[colorPairIndex][1];
                if (colorCode == colorPairCode)
                {
                    color = colorPairColor;
                    break; /* we have found our color, break the loop */
                }
            }
            /* print space with founded color background */
            Console_setCursorAttribute(color);
            putchar(' ');
        }
        break;
    }
    case 2: {
        
        Console_clear();
        for (i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++) {
                char colorCode = '\0';
                char color = '\0';
                /* get current color code from colorsPalette */
                colorCode = image[i][j];
                /* find corresponding color in table */
                for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++)
                {
                    char colorPairCode = colorsTable[colorPairIndex][0];
                    char colorPairColor = colorsTable[colorPairIndex][1];
                    if (colorCode == colorPairCode)
                    {
                        color = colorPairColor;
                        break; /* we have found our color, break the loop */
                    }
                }
                /* print space with founded color background */
                Console_setCursorAttribute(color);
                putchar(' ');
            }
            puts("");
        }
        Console_setCursorAttribute(BG_DEFAULT);
        break;
    }
    case 3: {
        int i, j;
        int x, y;
        int count = 0;
        int dir = 0;
        
        Console_clear();
        
            /* initial values */
            i = 27;
            j = 27;
            dir = 1;
            for (count = 0; count < size * size; count++) {
                /* output */
                x = 0 + j;
                y = 0 + i;
                Console_setCursorPosition(y, x);
                char colorCode = '\0';
                char color = '\0';
                /* get current color code from colorsPalette */
                colorCode = image[i][j];
                /* find corresponding color in table */
                for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++)
                {
                    char colorPairCode = colorsTable[colorPairIndex][0];
                    char colorPairColor = colorsTable[colorPairIndex][1];
                    if (colorCode == colorPairCode)
                    {
                        color = colorPairColor;
                        break; /* we have found our color, break the loop */
                    }
                }
                /* print space with founded color background */
                Console_setCursorAttribute(color);
                putchar(' ');
                fflush(stdout);  /* force console output */
                sleepMillis(40);
                /* end output */
        
                /* move next */
                i += dir;
                j -= dir;
        
                /* limits */
                if (i == -1) {
                    i++;
                    j -= 2;
                    dir = -dir;
                }
                if (j == -1) {
                    j++;
                    i -= 2;  
                    dir = -dir;
                }
                if (i == 28) {
                    i--;
                    dir = -dir;
                }
                if (j == 28) {
                    j--;
                    dir = -dir;
                }
        
            }
        
            Console_setCursorPosition( 1 + (size + 1), 1);
        break;
    }
    case 4: {
        int i, j;
        int x, y;
        int count = 0;
        int dir = 0;
        
        Console_clear();
        
            /* initial values */
            i = 27;
            j = 27;
            dir = 1;
            for (count = 0; count < size * size; count++) {
                /* output */
                x = 0 + j;
                y = 0 + i;
                Console_setCursorPosition(y, x);
                char colorCode = '\0';
                char color = '\0';
                /* get current color code from colorsPalette */
                colorCode = image[i][size - 1 -j];
                /* find corresponding color in table */
                for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++)
                {
                    char colorPairCode = colorsTable[colorPairIndex][0];
                    char colorPairColor = colorsTable[colorPairIndex][1];
                    if (colorCode == colorPairCode)
                    {
                        color = colorPairColor;
                        break; /* we have f ound our color, break the loop */
                    }
                }
                /* print space with founded color background */
                Console_setCursorAttribute(color);
                putchar(' ');
                fflush(stdout);  /* force console output */
                sleepMillis(40);
                /* end output */
        
                /* move next */
                i += dir;
                j -= dir;
        
                /* limits */
                if (i == -1) {
                    i++;
                    j -= 2;
                    dir = -dir;
                }
                if (j == -1) {
                    j++;
                    i -= 2;  
                    dir = -dir;
                }
                if (i == 28) {
                    i--;
                    dir = -dir;
                }
                if (j == 28) {
                    j--;
                    dir = -dir;
                }
        
            }
        
            Console_setCursorPosition( 1 + (size + 1), 1);
        break;
    }
    default:
        break;
    }
    puts("");
    Console_reset();
    return 0;
}