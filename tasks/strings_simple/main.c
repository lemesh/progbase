#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>
#include <string.h>
#include <ctype.h>

int main (void) {
    char line[] = "\n\n************************************************************************\n\n";
    char apartLine[] = "\n------------------------------------------------------------------------\n";
    int symbolCount = 0;
    char text[] = "Going through the forest is my favourite part of the walk Benji loves it too. There are rabbits to chase and old leaves to smell. Benji`s my dog, by the way, and I`m Grace. I live on a farm with my parents and take Benji for a walk most days after school. Dad doesn`t approve of me walking through the forest. 'Don`t talk to strangers,' he says. Though the truth is that there`s never anyone here. Just me, Benji and lots of rabbits and birds.While Benji runs ahead, I stop and take a photo of a butterfly that`s resting on a flower. A new Facebook photo?";

    int size = sizeof(text) / sizeof(text[0]);

    char vowels[] = "aeiou";
    char punctuationMarks[] = ".,!?'` ";

    puts(line);
    printf("1. Show all text and his size\n\n");
    for (int i = 0; i < size; i++) {
        printf("%c", text[i]);
        symbolCount++;
    }

    printf("\n\nCount of symbols -> %i", symbolCount);

    symbolCount = 0;

    puts(line);
    printf("2. Text without vowels\n\n");

    int status = 0;

    for (int i = 0; i < size; i++) {
        status = 0;
        for (int j = 0; j < 5; j++) {
           if (tolower(text[i]) == vowels[j]) {
               status = 1;
           } 
        }
        if (status != 1) {
            printf("%c", text[i]);
            symbolCount++;
        }
    }

    printf("\n\nCount of symbols -> %i", symbolCount);
    
    symbolCount = 0;

    puts(line);

    printf("3. All sentences apart\n\n");

    for (int i = 0; i < size; i++) {
        if (text[i] == '.' || text[i] == '?' || text[i] == '!') {
            printf("%c\t| Count of symbols -> %i", text[i], ++symbolCount);
            symbolCount = 0;
            puts(apartLine);
        } else {
            printf("%c", text[i]);
            symbolCount++;
        }
    }
    
    symbolCount = 0;
    
    puts(line);

    printf("4. Count of words\n");
    
    int wordsCount = 0;

    for (int i = 0; i < size; i++) {
        status = 0;
        for (int j = 0; j < sizeof(punctuationMarks) / sizeof(punctuationMarks[0]); j++) {
            if (text[i] == punctuationMarks[j])
                status = 1;
        }

        if (status == 1 && isalpha(text[i + 1])) {
            wordsCount++;
        }
    }

    printf("Count of words -> %i", wordsCount);

    puts(line);

    printf("5. Words begin in lowercase\n\n");

    wordsCount = 0;
    status = 0;
    
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < sizeof(punctuationMarks) / sizeof(punctuationMarks[0]); j++) {
           if (text[i] == punctuationMarks[j])
                status = 1;
        }

       // printf(" %i ", islower(text[i]));

        if (i == 0 && islower(text[i]) != 0)
            printf("%c", text[i]);
        else if (status == 1 && islower(text[i+1]) == 512) {
            printf("%c", text[i]);
            status = 2;
            wordsCount++;
        } else if (status == 1 && islower(text[i+1]) == 0)
            status = 3;
        else if (status == 1)
            printf("%c", text[i]);
        else if (status == 2)
            printf("%c", text[i]);
        
       //printf(" %i ", status);
    }

    printf("\n\nCount of words -> %i", wordsCount);
    
    puts(line);

    printf("6. Words, where number of vowels > consonants\n\n");

    wordsCount = 0;
    int lettersCount = 0;
    int vowelCount = 0;
    int consonantCount = 0;
    status = 0;

    for (int i = 0; i < size; i++) {
        status = 0;
        for (int j = 0; j < sizeof(punctuationMarks) / sizeof(punctuationMarks[0]); j++) {
            if (isalpha(text[i-1]) && (text[i] == punctuationMarks[j])) {
                status = 1;                
            }
        }

        //printf(" %i %i %i %i ||  ", status, lettersCount, vowelCount, consonantCount);

        if (status == 1) {
            if (vowelCount > consonantCount) {
                for (int j = lettersCount; j > 0; j--) {
                    printf("%c", text[i - j]);
                }
                printf(", ");
                wordsCount++;
            }
            vowelCount = 0;
            consonantCount = 0;
            lettersCount = 0;
        } else {
            for (int j = 0; j < 5; j++) {
                if (tolower(text[i]) == vowels[j]) {
                    vowelCount++;
                    status = 2;
                }
            }
            if (status != 2) {
                consonantCount++;
            }
            lettersCount++;
        }
    }

    printf("\n\nCount of words -> %i", wordsCount);
    
    puts(line);

    return 0;
}