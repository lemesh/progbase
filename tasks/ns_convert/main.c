#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

char * ns_convert(char * buffer, const char * number, unsigned int sourceBase, unsigned int destBase);
double convertToDecimal(const char * number, unsigned int sourceBase, int nonFloatLength);

int main(void) {
    char buf[1000] = "";
    char * x = NULL;
    int point = 0;
    int lengthToPoint = 0;
    x = ns_convert(buf, "1A", 2, 10);
    printf("Result - > ");
    for (int i = 0; i < strlen(x); i++) {
        point = 0;
        if (x[i] == '.') { 
            point = 1; 
            break;
        }
        lengthToPoint++;
    }

    if (point == 1) {
        for (int i = 0; i < lengthToPoint + 1; i++)
            printf("%c", x[i]);
    } else puts(x);

    puts("");
    x = ns_convert(buf, "1A.52C4", 18, 5);
    printf("Result - > ");

    lengthToPoint = 0;
    
    for (int i = 0; i < strlen(x); i++) {
        point = 0;
        if (x[i] == '.') { 
            point = 1; 
            break;
        }
        lengthToPoint++;
    }

    if (point == 1) {
        for (int i = 0; i < lengthToPoint + 7; i++)
            printf("%c", x[i]);
    } else puts(x);

    puts("");
    return 0;
}

char * ns_convert(char * buffer, const char * number, unsigned int sourceBase, unsigned int destBase) {
    bool error = false;

    for (int i = 0; i < strlen(number); i++) {
        if (number[i] >= 48 && number[i] <= 57) {
            if ((int)number[i] - 48 >= sourceBase)
                error = true;
        } else if (number[i] >= 65 && number[i] <= 90) {
            if((int)number[i] - 55 >= sourceBase)
                error = true;
        } else if (sourceBase < 2 || sourceBase > 36)
            error = true;
        else if (destBase < 2 || destBase > 36)
            error = true;
    }

    if (error == false) {
        int nonFloatLength = 0;
        double num = 0;
        //int pointPosition = 0;
        
        for (int i = 0; i < strlen(number); i++) {
            if (number[i] != '.')
                nonFloatLength++;
            else
                i = strlen(number);
        }
        //printf("Result > %f\n", convertToDecimal(number, sourceBase, nonFloatLength));
        num = convertToDecimal(number, sourceBase, nonFloatLength);

        if(destBase == 10) {
            sprintf(buffer, "%F", num);
            puts(buffer);
            return buffer;
        }

        int i = 0;
        double floatingPoint = num - (int)num;
        num  = (int)num;

        while(num >= 1) {
            if ((int)num % destBase >= 10)
                buffer[i] = (char)((int)num % destBase + 55);
            else
                buffer[i] = (char)((int)num % destBase + 48);
            //printf("%c\n", buffer[i]);
            num /= destBase;
            i++;
        }

        int k = i - 1;
        for(int j = 0; j < i / 2; j++) {
            char tmp = buffer[k];
            buffer[k] = buffer[j];
            buffer[j] = tmp;
            k--;
        }

        if (floatingPoint > 0) {
            buffer[i] = '.';
            //pointPosition = i;
            i++;
            //printf(".\n");
            for (int j = 0; j < 12; j++) {
                if (floatingPoint * destBase >= 10)
                    buffer[i] = (char)((int)(floatingPoint * destBase) + 55);
                else
                    buffer[i] = (char)((int)(floatingPoint * destBase) + 48);
                floatingPoint *= destBase;
                floatingPoint -= (int)floatingPoint;
                //printf("%c\n", buffer[i]);
                i++;
            }
        }

       // buffer[i+1] = '\0';
    } else return "/0";
    puts("");
    return buffer;
}

double convertToDecimal(const char * number, unsigned int sourceBase, int nonFloatLength) {
    int point = 0;
    double num = 0;
    double result = 0;
    int floatLength = -1;

    for (int i = 0; i < strlen(number); i++) {
        if (number[i] == '.') {
            point = 1;
            i++;
        }

        switch (point) {
            case 0:
                if (number[i] >= 65 && number[i] <= 90)
                    num = (int)number[i] - 55;
                else if (number[i] >= 48 && number[i] <= 57)
                    num = (int)number[i] - 48;

                result += num * pow(sourceBase, nonFloatLength - 1);
                nonFloatLength--;
                break;
            case 1:
                if (number[i] >= 65 && number[i] <= 90)
                    num = (int)number[i] - 55;
                else if (number[i] >= 48 && number[i] <= 57)
                    num = (int)number[i] - 48;

                result += num * pow(sourceBase, floatLength);
                floatLength--;
                break;
            default:
                break;
        }
    }
    return result;
}