#include <stdio.h>
#include <stdlib.h>

#define SIZE 6

struct Node {
    int digit;
    struct Node *node_part; 
};

void createNode(struct Node * newNode, int digit, int *count);

int main() {
    int digit = 0;
    int count = 0;
    struct Node newNode[SIZE];
    printf("Enter a new digit -> ");
    scanf("%i\n", &digit);
    getchar();
    for(int i = 0; i < SIZE; i++) {
        printf("Enter a new digit -> ");
        scanf("%i", &digit);
        createNode(&newNode[count], digit, &count);
    }
    return EXIT_SUCCESS;
}

void createNode(struct Node *newNode, int digit, int *count) {
    (*count)++;
    newNode->digit = digit;
    newNode->node_part = &newNode[*count];
}