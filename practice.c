#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>

int min2(int, int);
int min3(int, int, int);
double f(double, double);
int doubleEquals(double, double);
void printArr(int [], int);
void printMin(int [], int);
int minArr(int [], int);
void revertArrSigns(int [], int);
bool arrayEquals(int [], int, int [], int);
void myPuts(char []);
int myStrlen(char []);
bool stringEquals(char [], char []);

int main(void) { printf("\n---------------------------------------------------------------\n\n");
    assert(min2(4,2) == 2); // true
    assert(min2(1,3) == 1); //true
    assert(min3(2,4,1) == 1);   //true
    assert(min3(4,2,1) == 1);   //true
    assert(min3(4,1,2) == 1);   //true
    assert(fabs(f(3,4) - 9.237604) < 0.0001);   //true
    assert(isnan(f(0,4)));  //true

    printf("min -> %i\n", min2(4,2));
    printf("min -> %i\n", min3(2,4,1));
    printf("f -> %f\n", f(3, 4));

    printf("\n---------------------------------------------------------------\n\n");
    
    int a[] = {4, -5, 5, -4};
    int length = sizeof(a) / sizeof(a[0]);
    int b[] = {-4, 5, -5 , 4};
    int bLength = sizeof(b) / sizeof(b[0]);
    printArr(a, length);
    printMin(a, length);
    revertArrSigns(a, length);
    printArr(a, length);
    printMin(a, length);
    assert(arrayEquals(a, length, b, bLength));

    printf("\n---------------------------------------------------------------\n\n");

    assert(stringEquals("", "") == 1);
    assert(stringEquals("Hello", "") == 0);
    assert(stringEquals("hello", "Hello") == 0);
    assert(stringEquals("Hello", "Hello") == 1);
    assert(stringEquals("Hello", "Hello!") == 0);
    puts("");
    return 0;
}

void myPuts(char str[]) {
    puts(str);
    putchar('\n');
}

int myStrlen(char str[]) {
    return strlen(str);
}

bool stringEquals(char a[], char b[]) {
    if (myStrlen(a) != myStrlen(b))
        return false;
    else {
        for (int i = 0; i < myStrlen(a); i++) {
            if (a[i] != b[i])
                return false;
        }
    }
    return true;
}

int min2(int a, int b) {
    return (a > b) ? b : a;
}

int min3(int a, int b, int c) {
    return (min2(min2(a,b),c));
}

double f (double x, double y) {
    return x > 0 ? sqrt(1 / x) * pow(y, 2) : NAN;
}

int doubleEquals(double a, double b) {
    return fabs(a - b < 0.0000000001);
}

void printArr(int a[], int length) {
    for (int i = 0; i < length; i++)
        printf("%i, ", a[i]);
    puts("");
}

void printMin(int a[], int length) {
    printf("%i\n", minArr(a, length));
}

int minArr(int a[], int length) {
    int min = a[0];
    for (int i = 0; i < length - 1; i++)
        min = min2(min, a[i+1]);
    return min;
}

void revertArrSigns(int a[], int length) {
    for (int i = 0; i < length; i++)
        a[i] = -a[i];
}

bool arrayEquals(int a[], int length, int b[], int bLength) {
    if (length != bLength)
        return false;
    else {
        int i = 0;
        bool truth = true;
        while (i < length) {
            if (a[i] == b[i])
                i++;
            else
                return false;
        }
    }
    return true;    
}