#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

struct createVariable {
    char name[30];
    float value;
};

struct print {
    struct createVariable * var;
};

struct DichMLPlusPlusProgram {
    char command[100];
    struct createVariable * var;
};
 
struct DichMLPlusPlusProgram * newDichMLPlusPlusProgram(FILE * inputFile);
int runDichMLPlusPlusProgram(struct DichMLPlusPlusProgram * progra);
void freeDichMLPlusPlusProgram(struct DichMLPlusPlusProgram * program);
int fileExists(const char *fileName);
long getFileSize(FILE *f);
int readFileToBuffer(FILE *f, char *buffer, int bufferLength);
bool getFileName(char * buffer);

int main(int argc, char *argv[]) {
    if (argc < 2) return EXIT_FAILURE;
    if (getFileName(argv[1]) == false) return EXIT_FAILURE;
    if (fileExists(argv[1]) == 0) return EXIT_FAILURE;
    FILE* f = fopen(argv[1], "rb");
    newDichMLPlusPlusProgram(f);
    return EXIT_SUCCESS;
}

int fileExists(const char *fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // false: not exists
    return 1;  // true: exists
}

long getFileSize(FILE *f) {
    if (!f) return -1;  // error opening file
    fseek(f, 0, SEEK_END);  // rewind cursor to the end of file
    long fsize = ftell(f);  // get file size in bytes
    fseek(f, 0, SEEK_SET);
    return fsize;
}

int readFileToBuffer(FILE *f, char *buffer, int bufferLength) {
    if (!f) return 0;  // read 0 bytes from file
    long readBytes = fread(buffer, 1, bufferLength, f);
    buffer[readBytes] = '\0';
    return readBytes;  // number of bytes read
}

bool getFileName(char * buffer) {
    char buff[5];
    if (buffer[0] == '.' || buffer[0] == '\0') return false;
    for (int i = 0; buffer[i] != '\0'; i++) {
        if (buffer[i] == '.') {
            for (int j = 0; j < 5; j++) {
                i++;
                buff[j] = buffer[i];
            }
        }
    }
    if (!strcmp(buff, "dmlpp")) return true;
    else return false;
}

struct DichMLPlusPlusProgram * newDichMLPlusPlusProgram(FILE * inputFile) {
    struct DichMLPlusPlusProgram * commands = {NULL};
    long fsize = getFileSize(inputFile);
    char * buffer = malloc(fsize * sizeof(char));
    char lineBuffer[100];
    char command[10];
    char commandName[10];
    float value = 0.0;
    readFileToBuffer(inputFile, buffer, fsize);
    fclose(inputFile);
    int j = 0;
    for (int i = 0; buffer[i] != '\0';) {
        for (j = 0; buffer[j] != '\n' && buffer[j] != '\0'; j++) {
            lineBuffer[j] = buffer[i + j];
        }
        sscanf(lineBuffer, "%s%*c%[^:]%*c%f", command, commandName, &value);
        if (!strcmp(command, "val")) {
            commands = malloc(sizeof(struct DichMLPlusPlusProgram));
            strcpy(commands->var->name, commandName);
            commands->var->value = value;
        }
        i += (j + 1);
    }
    free(buffer);
    return commands;
}

/* int runDichMLPlusPlusProgram(struct DichMLPlusPlusProgram * program) {

} */